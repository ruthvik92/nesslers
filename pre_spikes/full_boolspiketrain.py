import pickle
import random
import timeit
import progressbar
path ='/home/ruthvik/DoGdata/'
output = open(path+'test_stimulus50000_Images150_separationmanual_1_sigma2_k_typesONspikelimit100.pkl','rb')
allspikes = pickle.load(output)
output.close()
interval_pre_spikes=[[] for i in range(len(allspikes))]
nofImages=50000
sigma=1
k=2
separation=150
spklmt = 100
tsteps = nofImages*separation

start = timeit.default_timer()
bar = progressbar.ProgressBar(maxval=len(allspikes), \
        widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()

for neurons in range(len(allspikes)):
    neuron_spikes = allspikes[neurons]

    for i in range(tsteps):
        low = (i/separation)*separation ##(i/separation) gives image number, into separation gives the left border of the image
        high = i                        ##i is the present timestep
        rangex = range(low,high+1)      ##this range is the range from start of the present image to present timestep
        interval_pre_spikes[neurons].append(not set(rangex).isdisjoint(neuron_spikes)) #see if dis is NOT the disjoint of neuron spikes
        bar.update(neurons)             ##if this is not disjoint it means that it has spiked in between the start of
                                        ##the image and the present timestep.
bar.finish()
time = timeit.default_timer()-start
print('time taken is {}').format(time)

picklefile3="ON_bool_spiketrains"+str(nofImages)+'_Images'+str(tsteps)+"_separation"+"manual_"+ str(sigma)+"_sigma"+str(k)+"_k_types"+"spklmt"+str(spklmt)+".pkl"
output3 = open(path+picklefile3,'wb')
pickle.dump(interval_pre_spikes,output3)
output3.close()
print "pickle file written to",path+picklefile3

