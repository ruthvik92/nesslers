import pickle
import random
import timeit
import progressbar
from sys import getsizeof
    
output = open('../DoGdata/test_stimulus50000_Images150_separationmanual_1_sigma2_k_typesONspikelimit100.pkl','rb')
allspikes = pickle.load(output)
output.close()
interval_pre_spikes=[]
nofImages=50000
separation=150
tsteps = nofImages*separation
#tsteps = 25
neuron_spikes = allspikes[177]
#neuron_spikes= [5,10,11,19]  #uncomment to see how it works and also set tsteps to 25 or 30 whatever
#neuron_spikes=[10]
#neuron_spikes=[]
bar = progressbar.ProgressBar(maxval=tsteps, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()

start = timeit.default_timer()
for i in range(tsteps):
    low = (i/separation)*separation ##i/separation gives image number, into separation gives the left border of the image
    high = i                        ##i is the present timestep
    rangex = range(low,high+1)      ##this range is the range from start of the present image to present timestep
    if(not set(rangex).isdisjoint(neuron_spikes)):
        interval_pre_spikes.append(i) #see if dis is NOT the disjoint of neuron spikes
    bar.update(i)                            ##if this is not disjoint it means that it has spiked in between the start of
time = timeit.default_timer()-start          #the image and the present timestep.
bar.finish()
print('time taken is {}').format(time)
print('No.Of bytes in interval_pre_spikes:{}').format(getsizeof(interval_pre_spikes))
    

