import numpy as np
import multiprocessing as mp
import sys
import pickle
import random
import timeit
import progressbar
from sys import getsizeof
NUM_PROCS=7
nofImages=50000
separation=150
spklmt=100
sigma=1
k=2
tsteps = nofImages*separation
output = open('/home/ruthvik/DoGdata/\
test_stimulus50000_Images150_separationmanual_1_sigma2_k_typesONspikelimit100.pkl','rb')
allspikes = pickle.load(output)
output.close()
path2 ='/home/ruthvik/DoGdata/para_inter_data/'
mat_size = len(allspikes)

def row_dist(procs):
    NUMPROCS = procs
    mybounds =[0]*NUMPROCS
    if(NUMPROCS>mat_size):
        sys.exit()
    q = int(mat_size/NUMPROCS)
    rem = int(mat_size%NUMPROCS)          #This block makes sure that rows are
    for j in range(q):                    #divided evenly across processes, no
        for i in range(NUMPROCS):         #process will get not more than 1 row
            mybounds[i]+=1                #compared to other processes. Make sure
    for i in range(rem):                  #that processes<=rows.
        mybounds[i]+=1
    bounds = []
    for i in range(len(mybounds)):
        bounds.append(sum(mybounds[:i+1]))
    bounds=[0]+bounds
    return bounds
bounds = row_dist(NUM_PROCS)

def proc_work(spikes,i):
    proc_num=i
    proc_allspikes = spikes
    proc_pre_spikes=[]
    for neurons in range(len(proc_allspikes)):
        neuron_spikes = proc_allspikes[neurons]
        
        for i in range(tsteps):
            low = (i/separation)*separation ##(i/separation) gives image number, into separation gives the left border of the image
            high = i                        ##i is the present timestep
            rangex = range(low,high+1)      ##this range is the range from start of the present image to present timestep
            if(not set(rangex).isdisjoint(neuron_spikes)):
                proc_pre_spikes[neurons].append(i)
                
    picklefile3="ON_pre_spikes_"+str(nofImages)+'_Images'+str(separation)+"_seprtn"+"man_"+ str(sigma)\
    +"_sigma"+str(k)+"_k_"+"spklmt"+str(spklmt)+"proc_"+str(proc_num)+".pkl"
    output3 = open(path2+picklefile3,'wb')
    pickle.dump(proc_pre_spikes,output3)
    output3.close()
    print "pickle file written to",path2+picklefile3



for i in range(NUM_PROCS):
    l = bounds[i]
    h = bounds[i+1]
    job = mp.Process(target = proc_work, args=[allspikes[l:h],i])
    job.start()
