"""This code will give indices of all timesteps where neuron has spiked between
    present timestep and the present image, this code will give a bool numpy array which states that
    if a neuron has ever spiked during the start of the image and present timestep, this is a demo of
    the code full_simple_boolspiketrain.py"""
import pickle
import random
import timeit
import progressbar
import numpy as np
import sys
from iteration_utilities import unique_everseen
#output = open('/home/ruthvik/DoGdata/test_stimulus50000_Images150_separationmanual_1_sigma2_k_typesONspikelimit100.pkl','rb')
#allspikes = pickle.load(output)
#output.close()
#interval_pre_spikes=[]
nofImages=50000
separation=150
separation=7
tsteps = nofImages*separation
tsteps = 25
#neuron_spikes = allspikes[100]
neuron_spikes= [5,6,10,11,19]  #uncomment to see how it works and also set tsteps to 25 or 30 whatever
#neuron_spikes=[10]
decisions = np.full([1,tsteps],False,dtype=bool)
new_neuron_spikes=[]
prev_range=[]
prev_no=0
prev_offset=0
i=0
bar = progressbar.ProgressBar(maxval=len(neuron_spikes), \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()


start = timeit.default_timer()
for items in neuron_spikes:
    no = items/separation
    offset = items%separation
    if items not in prev_range:
        rangex = range(items,(no+1)*separation)
        new_neuron_spikes.extend(rangex)    
    prev_range = rangex
    prev_no=no
    prev_offset=offset
    bar.update(i)
    i+=1

time = timeit.default_timer()-start
bar.finish()
print('time taken is {}').format(time)
start = timeit.default_timer()
for i in range(len(new_neuron_spikes)):
    decisions[0][new_neuron_spikes[i]]=True
time = timeit.default_timer()-start
print('time taken is {}').format(time)
#print('new_neuron_spikes:{}').format(new_neuron_spikes)
#print ' '    
#print('Bool file:{}').format(decisions)

