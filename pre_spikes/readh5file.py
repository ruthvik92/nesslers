"""This code reads the h5 file stored on the disk, the file in this code is bool
    data for the spikes that fired in between present time step and the beginning
    of the present image"""
import numpy as np
import h5py
import timeit
filename='/home/ruthvik/DoGdata/numpy_bool_\
prev_spks50000_Images7500000_separationmanual_1_sigma2_k_typesspklmt100.h5'
with h5py.File(filename, 'r') as hf:
    data = hf['Spike_numpy_bool'][:]

start = timeit.default_timer()
a_point = data[300][25000]
elapsed = timeit.default_timer()-start

print('Time elapsed:{}').format(elapsed)
