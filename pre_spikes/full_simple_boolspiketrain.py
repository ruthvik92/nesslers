"""This code will generate a bool numpy array that says if a neuron has spiked
    from present timestep to the beginning of the present image, use this one.
"""
import pickle
import random
import timeit
import progressbar
import numpy as np
from iteration_utilities import unique_everseen
import sys
import h5py
DEBUG=False

'''path = '/home/ruthvik/Desktop/DataSets/DoGdata/Freshdata/roundoff_data/train_60ts50st/'
output = open(path+'train_60000_Imgs10_mute_rndoff_1_sigma2_k_types_ON50_tstps.pkl','rb')
allspikes = pickle.load(output)
output.close()'''

## USE THIS IF YOU'RE FLATTENING THE MAPS OR ELSE USE THE ABOVE SECTION
path = '/home/ruthvik/Desktop/SCNFCN_Results/L123stat/v1/'
output = open(path+'spiketimes_pool1_60ts50st60000_Imgs9_maps.pkl','rb')
allspikes_dict = pickle.load(output)
output.close()
nofMaps = len(allspikes_dict)

allspikes = []
for i in range(1,nofMaps+1):
    items='map'+str(i)
    allspikes.extend(allspikes_dict[items])

if(DEBUG):
    print nofMaps
    print 'expected nofNeurons are:',49*9
    print 'nofNeurons are:',len(allspikes)
    sys.exit()
sigma=1
k=2
spklmt = 100
nofImages=60000
separation=60
tsteps = nofImages*separation
neurons = len(allspikes)

decisions = np.full([neurons,tsteps],False,dtype=bool)
#**new_neuron_spikes=[[] for i in range(neurons)]  ##this will have updated neuron spike ids within the image and present
                                                #timestep range
prev_range=[]  #This will contain the range from present timestep to end of this image, it will be used to prevent                                 
prev_no=0      #extra appending to new_neuron_spikes
prev_offset=0
start = timeit.default_timer()
bar = progressbar.ProgressBar(maxval=neurons, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()

for j in range(len(allspikes)):
    neuron_spikes = allspikes[j].as_array().astype(np.int) # UNCOMMENT THIS PART IF USING PYNN RECORDED SPIKETIMES
    #*start = timeit.default_timer()
    for items in neuron_spikes:
        #print int(items)
        no = int(items)/separation
        offset = int(items)%separation
        if items not in prev_range:
            rangex = range(int(items),(no+1)*separation)
            for things in rangex:
                decisions[j][things]=True
            #**new_neuron_spikes[j].extend(rangex)    
        prev_range = rangex
        prev_no=no
        prev_offset=offset
    bar.update(j)
    

time = timeit.default_timer()-start
bar.finish()
print('time taken is {}').format(time)


path ='/home/ruthvik/Desktop/SCNFCN_Results/L123stat/v1/'
path = '/home/ruthvik/Desktop/DataSets/DoGdata/Freshdata/roundoff_data/train_60ts50st/'
#**start = timeit.default_timer()
#**for j in range(neurons):
#**    for i in range(len(new_neuron_spikes)):
#**        decisions[j][new_neuron_spikes[i]]=True
#**time = timeit.default_timer()-start
#**print('time taken is {}').format(time)

#**picklefile3="ON_prev_spks"+str(nofImages)+'_Images'+str(tsteps)+"_separation"+"manual_"+ str(sigma)\
#**+"_sigma"+str(k)+"_k_types"+"spklmt"+str(spklmt)+".pkl"

#**output3 = open(path+picklefile3,'wb')
#**pickle.dump(new_neuron_spikes,output3)
#**output3.close()
#**print "pickle file written to",path+picklefile3

#**picklefile4="numpy_bool_prev_spks"+str(nofImages)+'_Images'+str(tsteps)+"_separation"+"manual_"+ str(sigma)\
#**+"_sigma"+str(k)+"_k_types"+"spklmt"+str(spklmt)+".pkl"

filename="numpy_bool_prev_spks_60ts50st"+str(nofImages)+'_Images'+".h5"

filename="numpy_bool_prev_spks_L3_60ts50st"+str(nofImages)+'_Images'+".h5"

with h5py.File(path+filename, 'w') as hf:
    hf.create_dataset("Spike_numpy_bool",  data=decisions)
    
print "H5py file written to",path+filename
#*output4 = open(path+picklefile4,'wb')
#*pickle.dump(decisions,output4)
#*output4.close()
#**print "pickle file written to",path+picklefile4
#print('new_neuron_spikes:{}').format(new_neuron_spikes)
#print ' '    
#print('Bool file:{}').format(decisions)

