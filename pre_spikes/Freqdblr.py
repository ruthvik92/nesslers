import numpy as np
import math
import matplotlib.pyplot as plt
import pdb



BENCHMARK = 0
TRAINING_SEQ_SIZE = 300

def test_examples():
    
    # Frequency Doubler
    seq = np.array(range(TRAINING_SEQ_SIZE))
    N_POINTS = 30;
    #seq_fd = np.array(range(TRAINING_SEQ_SIZE))
    #x_fd = np.array(range(TRAINING_SEQ_SIZE))
    x_fd = 2*math.pi*seq/N_POINTS
    input_fd = (np.sin(x_fd) + 1)/2;
    targets_fd = (np.sin(2*x_fd)+1)/2;
    return input_fd, targets_fd
    

if  BENCHMARK == 0:
    inputs, targets = test_examples()
    data = {}
    data['inputs']=inputs  ##Lets create a dictionary to pack the data to be saved.
    data['targets']=targets
#    plt.plot(inputs[0:300],'r')
#    plt.plot(targets[0:300],'b')
      
plt.figure(1)
plt.xlabel('seq')
plt.ylabel('Input Sequence')
plt.plot(inputs,'b')
plt.show
plt.figure(2)
plt.plot(targets,'r')
plt.xlabel('seq')
plt.ylabel('Targets')

import pickle
picklefile="sample"+".pkl" ##this will be the name of you file
path = "/home/ruthvik/Desktop/"  ## this is the path to the file.
file = open(path+picklefile,'wb') ##Note that the flag should be 'wb' for writing.
pickle.dump(data,file)  ##dump the data.
output.close()    ## ALWAYS close the file handle or else data will corrupt.


file = open(path+picklefile,'rb')##Note that the flag should be 'rb' for reading.
data = pickle.load(file)        ##You read the file.
file.close() #you close it ALWAYS.

plt.figure(1)
plt.xlabel('seq')
plt.ylabel('Input Sequence')
plt.plot(data['inputs'],'b')  ##our input is now data['inputs']
plt.show
plt.figure(2)
plt.plot(data['targets'],'r') ##our target is now data['targets']
plt.xlabel('seq')
plt.ylabel('Targets')


import h5py
filename="sample"+".h5"
file=h5py.File(path+filename,'w')  #write the file
group=file.create_group('data')
for key,value in data.items():
    group.create_dataset(key,data=value) ##create a group dataset with key, value pairs in the dict.

file=h5py.File(path+filename,'r')  ##read the file
a_group_key = list(file.keys())    ##read the group name
new_data=file[a_group_key[0]]         ##read all the data associated with a group

new_data_keys = list(new_data.keys())  ##make a list of keys associated with the read data.
plt.figure(1)
plt.xlabel('seq')
plt.ylabel('Input Sequence')
plt.plot(read_data[new_data_keys[0]],'b')  ##our input is now data['inputs']
plt.title(read_data_keys[0])
plt.show
plt.figure(2)
plt.plot(data[new_data_keys[1]],'r') ##our target is now data['targets']
plt.xlabel('seq')
plt.ylabel('Targets')
plt.title(read_data_keys[1])


