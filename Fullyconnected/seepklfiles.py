import pickle
import poisson_tools as pstool
import matplotlib.pyplot as plt
import numpy as np
import math
import random
ip_Neurons = 729
middle_Neurons = 100
path = '/home/ruthvik/Desktop/Fullyconnected_Results/15KImgs_v3_xhrs/'
pre_weightsfile ='ON_pre_weights_150tstp15000Imgs.pkl'
post_weightsfile ='ON_post_weights_150tstp_v3_15000Imgs.pkl'
spikefile = 'ON_middle_pop_spikes_v3_150tstp15000Imgs.pkl'

output = open(path+pre_weightsfile,'rb')
pre_weights = pickle.load(output)
output.close()

output = open(path+post_weightsfile,'rb')
post_weights = pickle.load(output)
output.close()

output = open(path+spikefile,'rb')
spikes = pickle.load(output)
output.close()

pstool.raster_plot_spike(spikes,'*')
plt.show()

weights = [np.empty(ip_Neurons) for i in range(middle_Neurons)]
for i in range(len(post_weights)):
    weights[i/ip_Neurons][i%ip_Neurons]=post_weights[i][2]
a = int(math.sqrt(ip_Neurons))
weights=[items.reshape(a,a) for items in weights]
a=random.sample(range(middle_Neurons),100)
for items in a:
    plt.imshow(weights[items],cmap='gray',interpolation='None')
    plt.show()

