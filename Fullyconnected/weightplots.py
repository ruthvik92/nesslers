import pickle
import poisson_tools as pstool
import matplotlib.pyplot as plt
import numpy as np
import math
import random
ip_Neurons = 729
middle_Neurons = 100
path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/500kImgs_70sep_again/'
#pre_weightsfile ='OFF_pre_weights_150tstp500000Imgs.pkl'
post_weightsfile ='ON_post_weights_70tstp_500000Imgs.pkl'
#spikefile = 'OFF_middle_pop_spikes_inh_20_150tstp500000Imgs.pkl'

#output = open(path+pre_weightsfile,'rb')
#pre_weights = pickle.load(output)
#output.close()

output = open(path+post_weightsfile,'rb')
post_weights = pickle.load(output)
output.close()

#output = open(path+spikefile,'rb')
#spikes = pickle.load(output)
#output.close()

#pstool.raster_plot_spike(spikes,'*')
#plt.show()

####Grabbing the weights from the list of tuples of length ip_Neurons*post_neurons(72900)
##### and dumping them to a numpy array of 729 length.
weights = [np.empty(ip_Neurons) for i in range(middle_Neurons)]
for i in range(len(post_weights)):
    weights[i/ip_Neurons][i%ip_Neurons]=post_weights[i][2]

##reshaping the numpy array to 27x27
a = int(math.sqrt(ip_Neurons))
weights=[items.reshape(a,a) for items in weights]


##Creating 100 axes in a pattern of 10x10 to plot 100 weights. 
fig, axes = plt.subplots(10, 10, figsize=(15, 15),
                         subplot_kw={'xticks': [], 'yticks': []})

fig.subplots_adjust(hspace=0.05, wspace=0.05)
axes = axes.flat
##Plotting interpolation='None'
for i in range(len(axes)):
    x = weights[i]
    axes[i].imshow(x,cmap='gray')
    #axes[i].set_title('Neuron'+str(i),fontsize=15)


#plt.suptitle('A_plus=0.008, A_minus=0.006,Tau_plus=40,Tau_minus=80,500k IP Images',fontsize=25)
plt.suptitle('Weights of 100 post synaptic neurons',fontsize=38)
plt.show()
