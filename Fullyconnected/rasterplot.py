import pickle
import poisson_tools as pstool
import matplotlib.pyplot as plt
import numpy as np
import math
import random
path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/500kImgs/final_test/'
spikefile = 'test_last_op_spikes_ON1028Imgs7_digit.pkl'
output = open(path+spikefile,'rb')
spikes = pickle.load(output)
output.close()

x = [i for i in range(len(spikes))]
plt.scatter(x,spikes)
#pstool.raster_plot_spike(spikes,'*')
plt.show()

#spikefile='train_svmip_spikes_ON50000Imgs.pkl'
#output = open(path+spikefile,'rb')
#spikes1 = pickle.load(output)
#output.close()


