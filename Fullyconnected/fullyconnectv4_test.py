""" This is the test code for the network learned in the fullyconnect.py, but there seems to be a problem with that code
so this code also doesnt work, write another test code without explicit inhibition neurons"""
import pyNN
import pyNN.neuron as p
import pickle
from pyNN.utility.plotting import Figure, Panel 
from pyNN.utility import ProgressBar
import numpy as np
image_size=27
nofImages=974 
image_duration=150
digit=8
t_stop = nofImages*image_duration
ip_Neurons = image_size*image_size
middle_neurons = 100
delay = 1.0
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 1,      #The refractory period in ms
                   'tau_syn_E' : 5,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 5,    #The inhibitory input current decay time-constant
                   'v_reset'   : -65,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -55  #The threshold voltage at which the neuron will spike.
                   }


timestep=1
p.setup(timestep=timestep)
################Load Stimulus Data #####################
ip_path= '/home/ruthvik/Desktop/DataSets/DoGdata/test_sep150/'
output=open(ip_path+'train_stimulus'+str(nofImages)+'_Images100_separationmanual_1_sigma2_k_typesON_spklmt100_digit'+str(digit)+'.pkl','rb')
allspikes = pickle.load(output)
output.close()
################Load weights ##########################
path= '/home/ruthvik/Desktop/Fullyconnected_Results//25KImgs_v4_28hrs/'
output = open(path+'ON_post_weights_150tstp_v4_25000Imgs.pkl','rb')
weights = pickle.load(output)
output.close()
### 3 types of populations defined ########
stimlus_pop = p.Population(ip_Neurons,p.SpikeSourceArray(spike_times= allspikes),label="driver")
middle_population =  p.Population(middle_neurons, p.IF_curr_exp,cell_params_lif, label='middle neurons')

#####2 static synapse between IP and middle pop and ######
stat_exc_learned = p.StaticSynapse(delay=delay)

#################Projections defined ######################
stimTomiddle = p.Projection(stimlus_pop,middle_population,p.FromListConnector(weights,safe = True,column_names=["weight"]),synapse_type = stat_exc_learned,receptor_type="excitatory")

middle_population.record('spikes')
class MyProgressBar(object):
    def __init__(self, interval, t_stop,population,projection):
        self.interval = interval
        self.t_stop = t_stop
        self.pb = ProgressBar(width=100, char="#")
        self.projection=projection
        self.population=population
        self.spiketimes = [[] for i in range(ip_Neurons)] 
        self.cellvoltages=[]
    def __call__(self, t):
        time_bin = int(t)/image_duration
        self.cell_voltages=self.population.get_membrane_potential()[:]
        neur_id = np.argmax(self.cell_voltages)
        if(self.cell_voltages[neur_id]>cell_params_lif['v_thresh']):
            self.spiketimes[neur_id].append(t)
        
        self.pb(t / self.t_stop)
        return t + self.interval
    def get_spiketimes(self):
        return self.spiketimes

data = MyProgressBar(timestep,t_stop,middle_population,stimTomiddle)
p.run(t_stop,callbacks=[data])
############################    layer  ##########################################
spikes = data.get_spiketimes()
picklefile3="test_ip_spikes_ON"+str(nofImages)+"Imgs"+str(digit)+"_digit"+".pkl"
output = open(path+picklefile3,'wb')
pickle.dump(spikes,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)
p.end()
