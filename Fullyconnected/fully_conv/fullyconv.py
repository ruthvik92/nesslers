import pyNN.neuron as p
from pyNN.utility import ProgressBar
from poolconvclassrndm_ver2 import *
import numpy as np
import random
from time import strftime, gmtime
from pyNN.utility import get_simulator, init_logging, normalized_filename
from pyNN.utility.plotting import DataTable
from pyNN.parameters import Sequence
from pyNN.utility.plotting import Figure, Panel
from copy import deepcopy
import sys
import pickle
import progressbar
import timeit
import operator
import pickle
image_size = 27
Neurons = image_size*image_size

kernel = int(raw_input('Enter Size of Kernel(n):'))
image_duration = int(raw_input('Enter separation in the inputs:'))
nofImages= int(raw_input('Enter No.Of input images to the network:'))
datagrab_inter =10000 
timestep= int(raw_input('Enter time step:'))
sampling_interval = int(raw_input('Enter sampling interval, better to keep it equal to timestep:'))
nofRuns = int(raw_input('Enter number of runs through the whole dataset:'))

data_grab =[datagrab_inter*i for i in range(0,nofImages/datagrab_inter)]
data_grab= [items*image_duration for items in data_grab]
A_plus = 0.008
A_minus = 0.006
tau_plus=40
tau_minus=80
delay = 1.0
nofImages_data=50000   ###To replicate the dataset
cell_params_lif = {'cm'        : 1, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 1,      #The refractory period in ms
                   'tau_syn_E' : 5,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 1,    #The inhibitory input current decay time-constant
                   'v_reset'   : -68,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -55  #The threshold voltage at which the neuron will spike.
                   }


cell_params_lif1 = {'cm'        : 0.35, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 5,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 0.1,      #The refractory period in ms
                   'tau_syn_E' : 1,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 15,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -50  #The threshold voltage at which the neuron will spike.
                   }


p.setup(timestep=timestep)
ipfile = '/home/ruthvik/Desktop/Summer 2017/nesslers/'
output = open(ipfile+'DoGdata/test_stimulus50000_Images150_separationmanual_1_sigma2_k_typesONspikelimit100.pkl','rb')
old_allspikes = pickle.load(output)
output.close()
allspikes = deepcopy(old_allspikes)

for runs in range(1,nofRuns+1):
    for i in range(len(allspikes)):
        allspikes[i].extend((np.array(old_allspikes[i])+nofImages_data*image_duration*runs).tolist())

del old_allspikes


#t_stop = nofImages*image_duration*nofRuns
t_stop = nofImages*image_duration

z = poolconv(kernel,27,0.8,0.05,delay,4)
z.PoolConv()
kernels = z.conn_list

# Stimulus population, used to connect to input population.
stimlus_pop = p.Population(Neurons,p.SpikeSourceArray(spike_times= allspikes),label="driver")

# Input population of 729 neurons.
ip_pop = p.Population((image_size-kernel+1)**2, p.IF_curr_exp,cell_params_lif, label = "input")

# Inhibitory neuron for input population.
inhpop_4ip = p.Population(1, p.IF_curr_exp, cell_params_lif1,label="Inhibitory4input")

#Connector from stimulus to input
stim_ip_connctn = p.FromListConnector(kernels,safe = True,column_names=["weight", "delay"])

# STDP synapse for stimulus pop to input populations.
stdp_model = p.STDPMechanism(
  timing_dependence=p.SpikePairRule(tau_plus=tau_plus, tau_minus=tau_minus,A_plus=A_plus, A_minus=A_minus),
  weight_dependence=p.AdditiveWeightDependence(w_min=0.0, w_max=1),delay=delay,
  dendritic_delay_fraction=0)

# Static synapses between inhibitory and input populations.
stat_inh = p.StaticSynapse(weight=-50, delay=delay/4.0)
stat_exc = p.StaticSynapse(weight=50, delay=delay/4.0)

#################Projections defined ######################

stimToip = p.Projection(stimlus_pop,ip_pop,stim_ip_connctn,synapse_type = stdp_model, receptor_type="excitatory")

ipToinhpop_4ip = p.Projection(ip_pop,inhpop_4ip,p.AllToAllConnector(allow_self_connections=False),synapse_type =stat_exc , receptor_type="excitatory")

inhpop_4ipToip = p.Projection(inhpop_4ip,ip_pop,p.AllToAllConnector(allow_self_connections=False),synapse_type = stat_inh, receptor_type="inhibitory")

ip_pop.record('spikes','v')

bar = progressbar.ProgressBar(maxval=t_stop, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/fully_convo/50kImgs/'
pre_weights = stimToip.get('weight',format='list',with_address=True)
picklefile1="OFF_pre_weights_150tstp"+str(t_stop/image_duration)+"Imgs"+".pkl"
output = open(path+picklefile1,'wb')
pickle.dump(pre_weights,output)
output.close()
print('before run weights pkl file saved at {}').format(path+picklefile1)

start = timeit.default_timer()
class WeightUpdate(object):

    def __init__(self, interval, projection):
        self.interval = interval
        self.projection =projection
    def __call__(self, t):
        time_bin = int(t)/image_duration
        if(t in data_grab):
            post_weights = self.projection.get('weight',format='list',with_address=True)
            picklefile="ON_post_weights_150tstp_"+str(time_bin)+"Imgs"+".pkl"
            output = open(path+picklefile,'wb')
            pickle.dump(post_weights,output)
            output.close()
            print('saved weights at image:{}').format(time_bin)


        bar.update(t+self.interval-1)
        return t + self.interval
weight_update = WeightUpdate(sampling_interval,stimToip )
p.run(t_stop,callbacks=[weight_update])
elapsed = timeit.default_timer()-start
bar.finish()
####################################################################################
####################### Extracting and Saving Weights #############################
post_weights = stimToip.get('weight',format='list',with_address=True)
picklefile2="ON_post_weights_150tstp_"+str(t_stop/image_duration)+"Imgs"+".pkl"
output = open(path+picklefile2,'wb')
pickle.dump(post_weights,output)
output.close()
print('after run weights pkl file saved at {}').format(path+picklefile2)

###################################################################################
######################Plotting and Spiketrains of middle##########################
############################    layer  ##########################################
data = ip_pop.get_data().segments[0]
spikes = data.spiketrains
picklefile3="ON_ip_pop_spikes_inh_20_150tstp"+str(t_stop/image_duration)+"Imgs"+".pkl"
output = open(path+picklefile3,'wb')
pickle.dump(spikes,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)
print('Time elapsed:{}').format(elapsed)

