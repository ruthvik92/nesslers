import pickle
import poisson_tools as pstool
import matplotlib.pyplot as plt
import numpy as np
import math
import random
ip_Neurons = 529
filter_size=25
path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/fully_convo/50kImgs/'
#pre_weightsfile ='OFF_pre_weights_150tstp500000Imgs.pkl'
post_weightsfile ='ON_post_weights_150tstp_5000Imgs.pkl'
spikefile = 'OFF_ip_pop_spikes_inh_20_150tstp5000Imgs.pkl'

#output = open(path+pre_weightsfile,'rb')
#pre_weights = pickle.load(output)
#output.close()

output = open(path+post_weightsfile,'rb')
post_weights = pickle.load(output)
output.close()

output = open(path+spikefile,'rb')
spikes = pickle.load(output)
output.close()

pstool.raster_plot_spike(spikes,'*')
plt.show()

final_weights = {}
for i in range(ip_Neurons):
    final_weights["Neuron"+str(i)]=[items[2] for items in post_weights[filter_size*i:filter_size*(i+1)]]


neurons=final_weights.keys()	
fig, axes = plt.subplots(int(math.sqrt(ip_Neurons)), int(math.sqrt(ip_Neurons)), figsize=(8, 8),
                         subplot_kw={'xticks': [], 'yticks': []})

#fig.subplots_adjust(hspace=0.3, wspace=0.05)

for ax, neuron in zip(axes.flat, neurons):
    x = final_weights[neuron]
    np_x = np.array(x)
    np_x = np_x.reshape(5,5)
    ax.imshow(np_x,cmap='gray',interpolation='None')
    ax.set_title(neuron,fontsize=10)

plt.show()
