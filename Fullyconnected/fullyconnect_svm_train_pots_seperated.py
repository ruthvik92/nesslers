""" This is the test code for the network learned in the fullyconnect.py, but there seems to be a problem with that code
so this code also doesnt work, write another test code without explicit inhibition neurons"""
import pyNN
import pyNN.neuron as p
import pickle
from pyNN.utility.plotting import Figure, Panel 
from pyNN.utility import ProgressBar
import numpy as np
image_size=27
nofImages = 10000
image_duration=150
digit=0
tsteps = nofImages*image_duration
ip_Neurons = image_size*image_size
middle_neurons = 100 
delay = 1.0 
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 1,      #The refractory period in ms
                   'tau_syn_E' : 1.25,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 15,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : float('Inf')  #The threshold voltage at which the neuron will spike.
                   }   

cell_params_lif1 = {'cm'        : 0.35, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 5,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 1,      #The refractory period in ms
                   'tau_syn_E' : 1,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 15,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -50  #The threshold voltage at which the neuron will spike.
                   }   




timestep=1
p.setup(timestep=timestep)
################Load Stimulus Data #####################
ip_path= '/home/ruthvik/Desktop/DataSets/DoGdata/separated_train_150/'
output=open(ip_path+'train_stimulus'+str(nofImages)+'_Images150_separationmanual_1_sigma2_k_typesON_spklmt150_digit'+str(digit)+'.pkl','rb')
allspikes = pickle.load(output)
output.close()
################Load weights ##########################
path= '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/500kImgs/'
output = open(path+'ON_post_weights_inh_20_150tstp500000Imgs.pkl','rb')
weights = pickle.load(output)
output.close()
### 3 types of populations defined ########
stimlus_pop = p.Population(ip_Neurons,p.SpikeSourceArray(spike_times= allspikes),label='driver')
middle_population =  p.Population(middle_neurons, p.IF_curr_exp,cell_params_lif, label='middle neurons')
inh_population = p.Population(middle_neurons, p.IF_curr_exp, cell_params_lif1,label='Inhibitory')

#####2 static synapse between IP and middle pop and ######
stat_exc_learned = p.StaticSynapse(delay=delay)
stat_inh = p.StaticSynapse(weight=-50, delay=delay/4.0)
stat_exc = p.StaticSynapse(weight=50, delay=delay/4.0)

#################Projections defined ######################
stimTomiddle = p.Projection(stimlus_pop,middle_population,p.FromListConnector(weights,safe =\
True,column_names=['weight']),synapse_type = stat_exc_learned,receptor_type='excitatory')
#middleToinh = p.Projection(middle_population,inh_population,p.AllToAllConnector(allow_self_connections=False),synapse_type =stat_exc , receptor_type="excitatory")
#inhTomiddle = p.Projection(inh_population,middle_population,p.AllToAllConnector(allow_self_connections=False),synapse_type = stat_inh, receptor_type="inhibitory")

middle_population.record('spikes')
path= '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/500kImgs/svm/'
class MyProgressBar(object):

    def __init__(self, interval, t_stop, population):
        self.interval = interval
        self.t_stop = t_stop
        self.pb = ProgressBar(width=100, char='#')
        self.population = population
        self.winner_cell =np.empty([nofImages,middle_neurons]) 
    def __call__(self, t): 
        if(t%image_duration==0):
            volts=self.population.get_membrane_potential()
            image_num=t/image_duration-1
            np_volts = np.array(volts)
            self.winner_cell[int(image_num)]=np_volts
            self.population.set_membrane_potential([(i,cell_params_lif['v_reset']) for i in \
                                                range(middle_neurons)])
        self.pb(t / self.t_stop)
        return t + self.interval
    def get_winners(self):
        return self.winner_cell

my_progress=MyProgressBar(timestep,tsteps,middle_population)
p.run(tsteps,callbacks=[my_progress])

############################    layer  ##########################################
#data = middle_population.get_data().segments[0]
#spikes = data.spiketrains
#picklefile3="train_op_spikes_ON"+str(nofImages)+"Imgs"+str(digit)+"_digit"+".pkl"
#output = open(path+picklefile3,'wb')
#pickle.dump(spikes,output)
#output.close()
#print('spikes are saved to {}').format(path+picklefile3)
winners = my_progress.get_winners()
picklefile3='train_midl_lyr_pots_ON'+str(nofImages)+'Imgs'+'_digit'+str(digit)+'.pkl'
output = open(path+picklefile3,'wb')
pickle.dump(winners,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)

p.end()

