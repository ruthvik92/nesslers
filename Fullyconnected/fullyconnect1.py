"""This code is the implementation of the network as in Nessler's paper here, STDP and inhibition are taken care by
pyNN, but there is a problem with that, in Nessler's paper only one of the output neurons fired at a timestep but 
according to this code, many neurons will spike in a timestep, paams in this one was used to generate balanced firing in
DoG data"""

import pyNN
import pyNN.neuron as p
import pickle
from pyNN.utility.plotting import Figure, Panel 
from pyNN.utility import ProgressBar
import timeit
import numpy as np
from copy import deepcopy
image_size=27
nofImages= 5001
nofRuns=1 ##these are number of extra runs
image_duration=50
tsteps = nofImages*image_duration
A_plus = 0.01
A_minus = 0.8*A_plus
tau_plus=10
tau_minus = 25
ip_Neurons = image_size*image_size
middle_neurons = 100
delay = 1.0
datagrab_inter =2501
data_grab =[datagrab_inter*i for i in range(0,nofRuns*nofImages/datagrab_inter)]
data_grab= [items*image_duration for items in data_grab]
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 2,      #The refractory period in ms
                   'tau_syn_E' : 2,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 2,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -50  #The threshold voltage at which the neuron will spike.
                   }

cell_params_lif1 = {'cm'        : 1, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 10,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 3,      #The refractory period in ms
                   'tau_syn_E' : 2,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 2,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -55  #The threshold voltage at which the neuron will spike.
                   }
timestep=1
print 'ATTENTION!!!!!!!!!!!!'
print('image_duration:{}, tau_plus:{}, tau_minus:{}, A_plus:{}, A_minus:{},timestep:{}').format(image_duration,tau_plus, tau_minus\
,A_plus,A_minus,timestep)

p.setup(timestep=timestep)
ipfile = '/home/ruthvik/Desktop/DataSets/PoissonData/simple/'
output = open(ipfile+'stimulus_cont50000Images30rate40dursep10.pkl','rb')
old_allspikes = pickle.load(output)
output.close()
allspikes = deepcopy(old_allspikes)

#for runs in range(1,nofRuns+1):
#    for i in range(len(allspikes)):
#        allspikes[i].extend((np.array(old_allspikes[i])+nofImages*image_duration*runs).tolist())

del old_allspikes

### 3 types of populations defined ########
stimlus_pop = p.Population(ip_Neurons,p.SpikeSourceArray(spike_times= allspikes),label="driver")
middle_population =  p.Population(middle_neurons, p.IF_curr_exp,cell_params_lif, label='middle neurons')
inh_population = p.Population(1, p.IF_curr_exp, cell_params_lif1,label="Inhibitory")
### Random weights for stim_pop tp middle_pop ################
weights = pyNN.random.RandomDistribution('lognormal',[0.3010,1])

weights1 = np.random.lognormal(6.86,2.75,[1,middle_neurons])
#####################Synapses defined #####################3
stdp_model = p.STDPMechanism(
  timing_dependence=p.SpikePairRule(tau_plus=tau_plus, tau_minus=tau_minus,A_plus=A_plus, A_minus=A_minus),
  weight_dependence=p.AdditiveWeightDependence(w_min=0.0, w_max=1),delay=delay,
  dendritic_delay_fraction=0,weight=weights)

stat_inh = p.StaticSynapse(weight=-50, delay=1)
stat_exc = p.StaticSynapse(weight=40, delay=1)
#################Projections defined ######################
stimTomiddle = p.Projection(stimlus_pop,middle_population,p.AllToAllConnector(allow_self_connections=False),synapse_type = stdp_model, receptor_type="excitatory")

middleToinh = p.Projection(middle_population,inh_population,p.AllToAllConnector(allow_self_connections=False),synapse_type =stat_exc , receptor_type="excitatory")

inhTomiddle =p.Projection(inh_population,middle_population,p.AllToAllConnector(allow_self_connections=False),synapse_type =stat_inh, receptor_type="inhibitory")

#################Saving weights of pre run ################
path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/poisson/'
pre_weights = stimTomiddle.get('weight',format='list',with_address=True)
picklefile1="poisson_pre_weights_"+str(image_duration)+"tstp"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile1,'wb')
pickle.dump(pre_weights,output)
output.close()
print('before run weights pkl file saved at {}').format(path+picklefile1)

middle_population.record('spikes')
inh_population.record('spikes')
start = timeit.default_timer()

class MyProgressBar(object):
	def __init__(self, interval, t_stop,population,projection):
		self.interval = interval
		self.t_stop = t_stop
		self.pb = ProgressBar(width=100, char="#")
		self.population = population
		self.projection = projection
	def __call__(self, t):
		time_bin = int(t/image_duration)
		#if(t/image_duration==0):
		#	self.population.set_membrane_potential([(i,cell_params_lif['v_reset']) for i in \
        #                    range(middle_neurons)])
		if(t in data_grab):
			post_weights = self.projection.get('weight',format='list',with_address=True)
			picklefile="poisson_post_weights_"+str(image_duration)+"tstp_"+str(time_bin)+"Imgs"+".pkl"
			output = open(path+picklefile,'wb')
			pickle.dump(post_weights,output)
			output.close()
			print('saved weights at image:{}').format(time_bin)	
		self.pb(t / self.t_stop)
		return t + self.interval


p.run(tsteps,callbacks=[MyProgressBar(timestep,tsteps,middle_population,stimTomiddle)])

elapsed = timeit.default_timer()-start
####################################################################################
####################### Extracting and Saving Weights #############################
post_weights = stimTomiddle.get('weight',format='list',with_address=True)
picklefile2="poisson_post_weights_"+str(image_duration)+"tstp_"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile2,'wb')
pickle.dump(post_weights,output)
output.close()
print('after run weights pkl file saved at {}').format(path+picklefile2)

###################################################################################
######################Plotting and Spiketrains of middle##########################
############################    layer  ##########################################
data = middle_population.get_data().segments[0]
spikes = data.spiketrains
picklefile3="poisson_middle_pop_spikes_"+str(image_duration)+"tstp_"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile3,'wb')
pickle.dump(spikes,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)
print('Time elapsed:{}').format(elapsed)

data = inh_population.get_data().segments[0]
spikes = data.spiketrains
picklefile3="poisson_inh_pop_spikes_"+str(image_duration)+"tstp_"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile3,'wb')
pickle.dump(spikes,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)
print('Time elapsed:{}').format(elapsed)
#vm = data.filter(name="v")[0]
#gsyn = data.filter(name="gsyn_exc")[0]
#Figure(
#        #Panel(vm, ylabel="Membrane potential (mV)"),
#        #Panel(gsyn, ylabel="Synaptic conductance (uS)"),
#        Panel(data.spiketrains, xlabel="Time (ms)", xticks=True),
#        annotations="Simulated with NEURON"
#    ).save("middle_pop_spktrns.eps")
p.end()
