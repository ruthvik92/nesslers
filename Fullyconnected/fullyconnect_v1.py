"""The differenc between this code and fulyconnect.py is that here, only 1 of the output is let to fire and it will
update the weights similar to Nesslers paper, but this code runs very slow because of on the go making of the mask,
we will have 729 queries(to find if the pre syn neurons has fired between the start of the present image and the 
present timestep)into the allspikes dataset because of 729 pre synaptic neruons, each of this query takes 1ms so
totally it takes 700ms as a result, it's very slow, for updating the weights numpy mask is used, all the pre syn
neuron weights that fired before the present timestep and after the image presentaion are masked and rest of the
neurons's weights are decreased as they didn't fire in that interval in next step the mask is negated and now the
weights exposed are that of those neurons that fired in the interval, so they are increased."""

import pyNN
import pyNN.neuron as p
import pickle
from pyNN.utility.plotting import Figure, Panel 
import progressbar
import sys
import numpy as np
import timeit
image_size=27
nofImages=1000 
image_duration=150
A_plus = 0.003125
A_minus = 0.85*A_plus
tau_plus=37
tau_minus = 67
ip_Neurons = image_size*image_size
middle_neurons = 100
delay = 1.0
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 1,      #The refractory period in ms
                   'tau_syn_E' : 5,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 5,    #The inhibitory input current decay time-constant
                   'v_reset'   : -65,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -55  #The threshold voltage at which the neuron will spike.
                   }
timestep=1
p.setup(timestep=timestep)
ipfile = '/home/ruthvik/Desktop/Summer 2017/nesslers/DoGdata/'
output = open(ipfile+'test_stimulus50000_Images150_separationmanual_1_sigma2_k_typesONspikelimit100.pkl','rb')
allspikes = pickle.load(output)
output.close()
t_stop = nofImages*image_duration
### 2 types of populations defined ########
stimlus_pop = p.Population(ip_Neurons,p.SpikeSourceArray(spike_times= allspikes),label="driver")
middle_population =  p.Population(middle_neurons, p.IF_curr_exp,cell_params_lif, label='middle neurons')
### Random weights for stim_pop tp middle_pop ################
weights = pyNN.random.RandomDistribution('uniform',[0.4,0.8])

#####################Synapses defined #####################3

stat_exc = p.StaticSynapse(weight=weights, delay=delay/4.0)

#################Projections defined ######################
stimTomiddle = p.Projection(stimlus_pop,middle_population,p.AllToAllConnector(allow_self_connections=False),synapse_type = stat_exc, receptor_type="excitatory")

#################Saving weights of pre run ################
path = '/home/ruthvik/Desktop/Fullyconnected_Results/'
pre_weights = stimTomiddle.get('weight',format='list',with_address=True)
picklefile1="ON_pre_weights_150tstp"+str(nofImages)+"Imgs"+".pkl"
output = open(path+picklefile1,'wb')
pickle.dump(pre_weights,output)
output.close()
print('before run weights pkl file saved at {}').format(path+picklefile1)
middle_population.record('spikes','v')

bar = progressbar.ProgressBar(maxval=t_stop, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()

def syna_map(syn,wei):
	syn.weight = wei

start_time = timeit.default_timer()
class WeightUpdate(object):
    """
    A callback which draws a progress bar in the terminal.
    """ 
    def __init__(self, interval, population, projection):
        self.interval = interval
        self.population = population
        self.cell_voltages = []
        self.projection =projection
        self.spiketimes = [[] for i in range(ip_Neurons)]
    def __call__(self, t):
        
        time_bin = int(t)/image_duration
        self.cell_voltages=self.population.get_membrane_potential()[:]
        #print len(self.cell_voltages)
        neur_id = np.argmax(self.cell_voltages)
        #print self.cell_voltages[neur_id]
        
        if(self.cell_voltages[neur_id]>cell_params_lif['v_thresh']):
            self.spiketimes[neur_id].append(t)	
            weights =self.projection.get('weight',format='list',with_address=False)[(ip_Neurons*neur_id):(ip_Neurons*(neur_id+1))]
            weights = np.array(weights)
            synapses = list(self.projection.connections)
            synapses = synapses[ip_Neurons*neur_id:ip_Neurons*(neur_id+1)]
            mask = [0]*weights.size
            start_time = timeit.default_timer()
            for i in range(0,weights.size):
                #key = weights[i][0]
                #start_time = timeit.default_timer()
                Q = [allspikes[i][x] for x in range(0,len(allspikes[i])) if image_duration*time_bin\
                <allspikes[i][x] <t]
                if(len(Q)!=0):
                    mask[i]=True
                else:
                    mask[i]=False
			
            mask = np.array(mask)
            weights_dec=np.ma.array(weights,mask=mask)
            weights_dec = weights_dec-A_minus*weights_dec*(1-weights_dec)
            ##Since, we already decreased weights now we have to increase the ones that were NOT decreased. 
            weights_inc=np.ma.array(weights,mask=~mask)
            weights_inc = weights_inc+A_plus*weights_inc*(1-weights_inc)
            weights = np.ma.array(weights_dec.filled(1) * weights_inc.filled(1), mask=(weights_dec.mask * weights_inc.mask))
            map(syna_map,synapses,weights)
        bar.update(t+self.interval-1)
		#print 'time step',t
        return t + self.interval



    def get_spiketimes(self):
        return self.spiketimes

weight_update = WeightUpdate(timestep,middle_population,stimTomiddle)
p.run(nofImages*image_duration,callbacks=[weight_update])
bar.finish()

spikes = weight_update.get_spiketimes()


elapsed=timeit.default_timer()-start_time
print 'elapsed time', elapsed
####################################################################################
####################### Extracting and Saving Weights #############################
post_weights = stimTomiddle.get('weight',format='list',with_address=True)
picklefile2="ON_post_weights_150tstp_v1"+str(nofImages)+"Imgs"+".pkl"
output = open(path+picklefile2,'wb')
pickle.dump(post_weights,output)
output.close()
print('after run weights pkl file saved at {}').format(path+picklefile2)

picklefile3="ON_middle_pop_spikes_v1_150tstp"+str(nofImages)+"Imgs"+".pkl"
output = open(path+picklefile3,'wb')
pickle.dump(spikes,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)
#vm = data.filter(name="v")[0]
#gsyn = data.filter(name="gsyn_exc")[0]
#Figure(
#        #Panel(vm, ylabel="Membrane potential (mV)"),
#        #Panel(gsyn, ylabel="Synaptic conductance (uS)"),
#        Panel(data.spiketrains, xlabel="Time (ms)", xticks=True),
#        annotations="Simulated with NEURON"
#    ).save("middle_pop_spktrns.eps")
p.end()
