import pickle
import poisson_tools as pstool
import matplotlib.pyplot as plt
import numpy as np
import math
import random
path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/500kImgs/dgtwse_trn_spks/'
syns = []
topn =5
for  i in range(10):    
    spikefile = 'train_op_spikes_ON'+str(i)+'_digit.pkl'
    output = open(path+spikefile,'rb')
    spikes = pickle.load(output)
    output.close()
    lengths = np.array([items.size for items in spikes])
    print lengths[np.argsort(-lengths)[:5]]    
    pre_indices=np.argsort(-lengths)[:topn].tolist()
    #lengths.sort()
    print pre_indices
    #weight = 
    a_syn =[(items,i,float(lengths[items])/lengths[pre_indices[0]]) for items in pre_indices]
    syns.extend(a_syn)

print syns

#picklefile3="syns_lyr2-3_ON"+".pkl"
#output = open(path+picklefile3,'wb')
#pickle.dump(syns,output)
#output.close()
#print('spikes are saved to {}').format(path+picklefile3)
