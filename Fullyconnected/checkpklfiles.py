import pickle
import poisson_tools as pstool
import matplotlib.pyplot as plt
import numpy as np
import math
import random
exc_neurons=729
inh_neurons=exc_neurons/4
path = '/home/ruthvik/Desktop/Summer 2017/nesslers/Fullyconnected/Results/20180305/'
spikefile1 = 'VAbenchmarks_CUBA_exc_neuron_np1_20180305-185914.pkl'
output = open(path+spikefile1,'rb')
exc = pickle.load(output)
output.close()
exc = exc.segments[0].spiketrains

spikefile2 = 'VAbenchmarks_CUBA_inh_neuron_np1_20180305-185914.pkl'
output = open(path+spikefile2,'rb')
inh = pickle.load(output)
inh = inh.segments[0].spiketrains
output.close()

pstool.raster_plot_spike(exc,'*',markersize=4,color='b')

pstool.raster_plot_spike(inh,'^',markersize=8,color='r')
plt.show()
