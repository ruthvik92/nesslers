import pickle
import poisson_tools as pstool
import matplotlib.pyplot as plt
import numpy as np
import math
import random
exc_neurons=729
inh_neurons=exc_neurons/4
path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/poisson/'
spikefile1 = 'inh_exc_spk_rates_50tstp_50Imgs.pkl'
output = open(path+spikefile1,'rb')
spikerate = pickle.load(output)
output.close()

a=spikerate[0]
A=[a[i+1]-a[i] for i in range(len(a)-1)]
A=[items*exc_neurons for items in A]


b=spikerate[1]
B=[b[i+1]-b[i] for i in range(len(b)-1)]
B=[items*inh_neurons for items in B]
