"""This code is the implementation of the network as in Nessler's paper here, STDP and inhibition are taken care by
pyNN, but there is a problem with that, in Nessler's paper only one of the output neurons fired at a timestep but 
according to this code, many neurons will spike in a timestep, paams in this one was used to generate balanced firing in
DoG data"""

import pyNN
import pyNN.neuron as p
import pickle
import progressbar
from pyNN.utility.plotting import Figure, Panel 
from pyNN.utility import ProgressBar
import timeit
import numpy as np
from copy import deepcopy
image_size=27
nofImages=50 
nofRuns=1 ##these are number of extra runs
image_duration=50
A_plus = 0.01
A_minus = A_plus
tau_plus=10
tau_minus = 25
ip_Neurons = image_size*image_size
middle_neurons = ip_Neurons
inh_neurons=middle_neurons/4
delay = 1.0
p_e2i=0.575
p_i2e=0.6
p_i2i=0.5
datagrab_inter =5000
data_grab =[datagrab_inter*i for i in range(0,nofRuns*nofImages/datagrab_inter)]
data_grab= [items*image_duration for items in data_grab]
cell_params_lif = {'cm'        : 0.1775, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 28.4,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 10,      #The refractory period in ms
                   'tau_syn_E' : 30,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 40,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -68,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -38  #The threshold voltage at which the neuron will spike.
                   }

cell_params_lif1 = {'cm'        : 0.1775, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 28.4,#18,#28,#18,#9.3     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 3,      #The refractory period in ms
                   'tau_syn_E' : 30,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 40,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -68,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -38  #The threshold voltage at which the neuron will spike.
                   }
#cell_params_lif1 = {'cm'        : 0.096, # nF #capacitance of LIF neuron in nF
#                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
#                   'tau_m'     : 32,#18,#28,#18,#9.3     #The time-constant of the RC circuit, in ms
#                   'tau_refrac': 10,      #The refractory period in ms
#                   'tau_syn_E' : 12,    #The excitatory input current decay time-constant
#                   'tau_syn_I' : 17,    #The inhibitory input current decay time-constant
#                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
#                   'v_rest'    : -67,  #The ambient rest voltage of the neuron
#                   'v_thresh'  : -37  #The threshold voltage at which the neuron will spike.
#                   }
timestep=1
tsteps = nofRuns*nofImages*image_duration/timestep
t_stop=nofRuns*nofImages*image_duration
print 'ATTENTION!!!!!!!!!!!!'
print('image_duration:{}, tau_plus:{}, tau_minus:{}, A_plus:{}, A_minus:{},timestep:{}').format(image_duration,tau_plus, tau_minus\
,A_plus,A_minus,timestep)

p.setup(timestep=timestep)
ipfile = '/home/ruthvik/Desktop/DataSets/PoissonData/simple/'
output = open(ipfile+'stimulus_cont50000Images20rate40dursep10.pkl','rb')
old_allspikes = pickle.load(output)
output.close()

allspikes = deepcopy(old_allspikes)
#for runs in range(1,nofRuns+1):
#    for i in range(len(allspikes)):
#        allspikes[i].extend((np.array(old_allspikes[i])+nofImages*image_duration*runs).tolist())
del old_allspikes

### 3 types of populations defined ########
stimlus_pop = p.Population(ip_Neurons,p.SpikeSourceArray(spike_times= allspikes),label="driver")
middle_population =  p.Population(middle_neurons, p.IF_curr_exp,cell_params_lif, label='middle neurons')
inh_population = p.Population(inh_neurons, p.IF_curr_exp, cell_params_lif1,label="Inhibitory")
### Random weights for stim_pop tp middle_pop ################
weights = pyNN.random.RandomDistribution('uniform',[0.01,1])
delays = pyNN.random.RandomDistribution('uniform',[0,10])
#delays= 2
#####################Synapses defined #####################3
stdp_model = p.STDPMechanism(
  timing_dependence=p.SpikePairRule(tau_plus=tau_plus, tau_minus=tau_minus,A_plus=A_plus, A_minus=A_minus),
  weight_dependence=p.AdditiveWeightDependence(w_min=0.0, w_max=1),delay=delays,
  dendritic_delay_fraction=0,weight=weights)

stat_exc2inh = p.StaticSynapse(weight=0.75, delay=0)
stat_inh2exc = p.StaticSynapse(weight=-1.86, delay=0)
#stat_inh2inh = p.StaticSynapse(weight=-8, delay=1)
#################Projections defined ######################
stimTomiddle = p.Projection(stimlus_pop,middle_population,p.AllToAllConnector(allow_self_connections=False),synapse_type = stdp_model, receptor_type="excitatory")

middleToinh =p.Projection(middle_population,inh_population,p.FixedProbabilityConnector(p_e2i,allow_self_connections=False),synapse_type=stat_exc2inh,receptor_type="excitatory")

inhTomiddle=p.Projection(inh_population,middle_population,p.FixedProbabilityConnector(p_i2e,allow_self_connections=False),synapse_type=stat_inh2exc, receptor_type="inhibitory")

#inhToinh=p.Projection(inh_population,inh_population,p.FixedProbabilityConnector(p_i2i,allow_self_connections=False),synapse_type=stat_inh2inh, receptor_type="inhibitory")
#################Saving weights of pre run ################
path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/poisson/'
pre_weights = stimTomiddle.get('weight',format='list',with_address=True)
picklefile1="poisson_pre_weights_"+str(image_duration)+"tstp"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile1,'wb')
pickle.dump(pre_weights,output)
output.close()
print('before run weights pkl file saved at {}').format(path+picklefile1)

bar = progressbar.ProgressBar(maxval=tsteps, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()

middle_population.record('spikes')
inh_population.record('spikes')
start = timeit.default_timer()

class MyProgressBar(object):
    def __init__(self, interval,population,projection,inh_population):
        self.interval = interval
        self.population = population
        self.projection = projection
        self.inh_population = inh_population
        self.exc_rate=[]
        self.inh_rate=[]
    def __call__(self, t):
        time_bin = int(t/image_duration)
        #if(t in data_grab):
        #    post_weights = self.projection.get('weight',format='list',with_address=True)
        #    picklefile="poisson_post_weights_"+str(image_duration)+"tstp_"+str(time_bin)+"Imgs"+".pkl"
        #    output = open(path+picklefile,'wb')
        #    pickle.dump(post_weights,output)
        #    output.close()
        #    print('saved weights at image:{}').format(time_bin)
        #    E_count = self.population.mean_spike_count()
        #    I_count = self.inh_population.mean_spike_count()
        #    print("Excitatory rate        : %g Hz" % (E_count * 1000.0 / t_stop,))
        #    print("Inhibitory rate        : %g Hz" % (I_count * 1000.0 / t_stop,))
        
        if(t%image_duration==0):
            E_count = self.population.mean_spike_count()
            I_count = self.inh_population.mean_spike_count()
            self.exc_rate.append(E_count)
            self.inh_rate.append(I_count)

        #print t
        bar.update(t)
        return t + self.interval

    def getSpikeRates(self):
        return self.exc_rate, self.inh_rate

my_progress = MyProgressBar(timestep,middle_population,stimTomiddle,inh_population)
p.run(tsteps,callbacks=[my_progress])
bar.finish()
#p.run(t_stop)
elapsed = timeit.default_timer()-start
####################################################################################
####################### Extracting and Saving Weights #############################
post_weights = stimTomiddle.get('weight',format='list',with_address=True)
picklefile2="poisson_post_weights_"+str(image_duration)+"tstp_"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile2,'wb')
pickle.dump(post_weights,output)
output.close()
print('after run weights pkl file saved at {}').format(path+picklefile2)

###################################################################################
######################Plotting and Spiketrains of middle##########################
############################    layer  ##########################################
data = middle_population.get_data().segments[0]
spikes = data.spiketrains
picklefile3="poisson_middle_pop_spikes_"+str(image_duration)+"tstp_"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile3,'wb')
pickle.dump(spikes,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)
print('Time elapsed:{}').format(elapsed)

data = inh_population.get_data().segments[0]
spikes = data.spiketrains
picklefile3="poisson_inh_pop_spikes_"+str(image_duration)+"tstp_"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile3,'wb')
pickle.dump(spikes,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)
print('Time elapsed:{}').format(elapsed)

#E_count = middle_population.mean_spike_count()
#I_count = inh_population.mean_spike_count()
#print("Excitatory rate        : %g Hz" % (E_count * 1000.0 / t_stop,))
#print("Inhibitory rate        : %g Hz" % (I_count * 1000.0 / t_stop,))

exc_rates,inh_rates = my_progress.getSpikeRates()
spike_rates=[exc_rates,inh_rates]
picklefile4="inh_exc_spk_rates_"+str(image_duration)+"tstp_"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile4,'wb')
pickle.dump(spike_rates,output)
output.close()
print('spike rates are saved to {}').format(path+picklefile4)
print('Time elapsed:{}').format(elapsed)
