# coding: utf-8
"""
Balanced network of excitatory and inhibitory neurons.

An implementation of benchmarks 1 and 2 from

    Brette et al. (2007) Journal of Computational Neuroscience 23: 349-398

The network is based on the CUBA and COBA models of Vogels & Abbott
(J. Neurosci, 2005).  The model consists of a network of excitatory and
inhibitory neurons, connected via current-based "exponential"
synapses (instantaneous rise, exponential decay).


Usage: python VAbenchmarks.py [-h] [--plot-figure] [--use-views] [--use-assembly]
                              [--use-csa] [--debug DEBUG]
                              simulator benchmark

positional arguments:
  simulator       neuron, nest, brian or another backend simulator
  benchmark       either CUBA or COBA

optional arguments:
  -h, --help      show this help message and exit
  --plot-figure   plot the simulation results to a file
  --use-views     use population views in creating the network
  --use-assembly  use assemblies in creating the network
  --use-csa       use the Connection Set Algebra to define the connectivity
  --debug DEBUG   print debugging information


Andrew Davison, UNIC, CNRS
August 2006

"""

import socket
import pyNN
from math import *
from pyNN.utility import get_simulator, Timer, ProgressBar, init_logging, normalized_filename
from pyNN.random import NumpyRNG, RandomDistribution
import pickle
sim, options = get_simulator(
                    ("benchmark", "either CUBA or COBA"),
                    ("--plot-figure", "plot the simulation results to a file", {"action": "store_true"}),
                    ("--use-views", "use population views in creating the network", {"action": "store_true"}),
                    ("--use-assembly", "use assemblies in creating the network", {"action": "store_true"}),
                    ("--use-csa", "use the Connection Set Algebra to define the connectivity", {"action": "store_true"}),
                    ("--debug", "print debugging information"))
# ===== Load input data =====================================================
poisson_ipfile = '/home/ruthvik/Desktop/DataSets/PoissonData/simple/'
poisson='stimulus_cont50000Images20rate40dursep10.pkl'

dog_ipfile = '/home/ruthvik/Desktop/DataSets/DoGdata/'
dog = 'test_stimulus50000_Images100_separationmanual_1_sigma2_k_typesOFFspikelimit100.pkl'

output = open(poisson_ipfile+poisson,'rb')
allspikes = pickle.load(output)
output.close()

# === Configure the simulator ================================================


timer = Timer()

# === Define parameters ========================================================

threads  = 1
rngseed  = 98765
parallel_safe = True

n        = 911  # number of cells
r_ei     = 4.0   # number of excitatory cells:number of inhibitory cells
pconn    = 0.02  # connection probability

e2i_pconn=0.575
i2i_pconn=0.6
i2e_pconn=0.6

stim_dur = 50.   # (ms) duration of random stimulation
rate     = 100.  # (Hz) frequency of the random stimulation

dt       = 1   # (ms) simulation timestep
image_duration=50
nofImages=100
nofRuns=1
tstop    = image_duration*nofImages  # (ms) simulaton duration
delay    = 2

'''
 If you set all the a_eta parameters and all but one of the a_gamma parameters to zero, you should get an adaptive
 threshold with single exponential decay - the non-zero a_gamma parameter will give you the change in threshold per
 spike, the corresponding tau_gamma parameter controls the decay time constant.
'''
a_eta1=a_eta2=a_eta3=0
a_gamma1=a_gamma2=0
a_gamma3=10
tau_gamma3=3

### what is the synaptic delay???
# STDP parameters
A_plus = 0.001
A_minus = A_plus
tau_plus=25
tau_minus = 25

# === Calculate derived parameters =============================================

n_exc = int(round((n*r_ei/(1+r_ei)))) # number of excitatory cells
n_inh = n - n_exc                     # number of inhibitory cells
exc_celltype = sim.IF_cond_exp
inh_celltype=sim.IF_cond_exp
# === Build the network ========================================================

extra = {'threads' : threads,
         'filename': "va_%s.xml" % options.benchmark,
         'label': 'VA'}
if options.simulator == "neuroml":
    extra["file"] = "VAbenchmarks.xml"

node_id = sim.setup(timestep=dt, min_delay=delay, max_delay=10.0, **extra)
np = sim.num_processes()

host_name = socket.gethostname()
print("Host #%d is on %s" % (node_id + 1, host_name))

print("%s Initialising the simulator with %d thread(s)..." % (node_id, extra['threads']))

cell_params = {
    'tau_m'      : 28,    'tau_syn_E'  : 5,  'tau_syn_I'  :5,
    'v_rest'     : -68,   'v_reset'    : -70,  
    'cm'         : 0.1775,       'tau_refrac' : 3, 'a_eta1':1, 'a_eta2':1,
    'a_eta3':1, 'a_gamma1':a_gamma1, 'a_gamma2':a_gamma2, 'a_gamma3':a_gamma3, 'tau_gamma3':tau_gamma3}

cell_params = {
    'tau_m'      : 28,    'tau_syn_E'  : 5,  'tau_syn_I'  :5,
    'v_rest'     : -68,   'v_reset'    : -70,  
    'cm'         : 0.1775,       'tau_refrac' : 3}

cell_params_inh = {
    'tau_m'      : 10,    'tau_syn_E'  : 10,  'tau_syn_I'  : 10,
    'v_rest'     : -68,   'v_reset'    : -70,  'v_thresh': -37,
    'cm'         : 0.1,       'tau_refrac' : 3}

timer.start()

print("%s Creating cell populations..." % node_id)
# create separate populations for excitatory and inhibitory neurons
exc_cells = sim.Population(n_exc, exc_celltype(**cell_params), label="Excitatory_Cells")
inh_cells = sim.Population(n_inh, inh_celltype(**cell_params_inh), label="Inhibitory_Cells")

ext_stim=sim.Population(n_exc,sim.SpikeSourceArray(spike_times= allspikes),label="driver")
ext_conn = sim.AllToAllConnector(allow_self_connections=False)
weights = pyNN.random.RandomDistribution('uniform',[0.00001,.001])
delays = pyNN.random.RandomDistribution('uniform',[0,10])
#####################Synapses defined #####################3
ext_syn = sim.STDPMechanism(
  timing_dependence=sim.SpikePairRule(tau_plus=tau_plus, tau_minus=tau_minus,A_plus=A_plus, A_minus=A_minus),
    weight_dependence=sim.AdditiveWeightDependence(w_min=0.0, w_max=.001),delay=delays,
      dendritic_delay_fraction=0,weight=weights)

print("%s Initialising membrane potential to random values..." % node_id)
rng = NumpyRNG(seed=rngseed, parallel_safe=parallel_safe)
uniformDistr = RandomDistribution('uniform', low=-70, high=-60, rng=rng)
exc_cells.initialize(v=uniformDistr)
inh_cells.initialize(v=uniformDistr)

print("%s Connecting populations..." % node_id)
progress_bar = ProgressBar(width=20)
exc2inh_connector = sim.FixedProbabilityConnector(e2i_pconn, rng=rng, callback=progress_bar)
inh2exc_connector = sim.FixedProbabilityConnector(i2e_pconn, rng=rng, callback=progress_bar)
inh2inh_connector = sim.FixedProbabilityConnector(i2i_pconn, rng=rng, callback=progress_bar)

exc2inh_syn = sim.StaticSynapse(weight=0.002, delay=0) #was set to delay
inh2exc_syn = sim.StaticSynapse(weight=-0.005, delay=0) #was set to delay
inh2inh_syn = sim.StaticSynapse(weight=-.01, delay=0) #was set to delay
connections={}
connections['ext2e'] = sim.Projection(ext_stim, exc_cells, ext_conn, ext_syn, receptor_type='excitatory')
connections['e2i'] = sim.Projection(exc_cells, inh_cells, exc2inh_connector, exc2inh_syn, receptor_type='excitatory')
connections['i2e'] = sim.Projection(inh_cells, exc_cells, inh2exc_connector, inh2exc_syn, receptor_type='inhibitory')
connections['i2i'] = sim.Projection(inh_cells, inh_cells, inh2inh_connector, inh2inh_syn, receptor_type='inhibitory')

# === Setup recording ==========================================================
print("%s Setting up recording..." % node_id)
exc_cells.record('spikes')
inh_cells.record('spikes')
exc_cells[0, 1].record('v')

buildCPUTime = timer.diff()

# === Save connections to file =================================================

#for prj in connections.keys():
    #connections[prj].saveConnections('Results/VAbenchmark_%s_%s_%s_np%d.conn' % (benchmark, prj, options.simulator, np))
saveCPUTime = timer.diff()

# === Run simulation ===========================================================

print("%d Running simulation..." % node_id)

sim.run(tstop)

simCPUTime = timer.diff()

E_count = exc_cells.mean_spike_count()
I_count = inh_cells.mean_spike_count()

# === Print results to file ====================================================

print("%d Writing data to file..." % node_id)

filename = normalized_filename("Results", "VAbenchmarks_%s_exc" % options.benchmark, "pkl",
                               options.simulator, np)
exc_cells.write_data(filename,
                     annotations={'script_name': __file__})
inh_cells.write_data(filename.replace("exc", "inh"),
                     annotations={'script_name': __file__})

writeCPUTime = timer.diff()

path = '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/poisson/'
post_weights = connections["ext2e"].get("weight",format="list",with_address=True)
picklefile2="poisson_balnc_weights_"+str(image_duration)+"tstp_"+str(nofRuns*nofImages)+"Imgs"+".pkl"
output = open(path+picklefile2,'wb')
pickle.dump(post_weights,output)
output.close()
print('after run weights pkl file saved at {}').format(path+picklefile2)

connections = u"%d ext→e  %d e→i  %d i→e  %d i→i" %   (connections['ext2e'].size(),
                                                       connections['e2i'].size(),
                                                       connections['i2e'].size(),
                                                       connections['i2i'].size())







if node_id == 0:
    print("\n--- Vogels-Abbott Network Simulation ---")
    print("Nodes                  : %d" % np)
    print("Simulation type        : %s" % options.benchmark)
    print("Number of Neurons      : %d" % n)
    print("Number of Synapses     : %s" % connections)
    print("Excitatory rate        : %g Hz" % (E_count * 1000.0 / tstop,))
    print("Inhibitory rate        : %g Hz" % (I_count * 1000.0 / tstop,))
    print("Build time             : %g s" % buildCPUTime)
    #print("Save connections time  : %g s" % saveCPUTime)
    print("Simulation time        : %g s" % simCPUTime)
    print("Writing time           : %g s" % writeCPUTime)

if options.plot_figure:
	from pyNN.utility.plotting import Figure, Panel
	figure_filename = filename.replace("pkl", "png")
	Figure(
        Panel(exc_cells.get_data().segments[0].filter(name='v')[0],
              ylabel="Membrane potential (mV)",
              data_labels=[exc_cells.label], yticks=True, ylim=(-75, -10)),
        title="Responses of standard neuron models to current injection",
        annotations="Simulated with %s" % options.simulator.upper()
    ).save(figure_filename)
	print(figure_filename)
# === Finished with simulator ==================================================
sim.end()
