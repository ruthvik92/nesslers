""" This is the test code for the network learned in the fullyconnect.py, but there seems to be a problem with that code
so this code also doesnt work, write another test code without explicit inhibition neurons"""
import pyNN
import pyNN.neuron as p
import pickle
from pyNN.utility.plotting import Figure, Panel 
from pyNN.utility import ProgressBar
image_size=27
nofImages=1010
image_duration=150
digit=3
ip_Neurons = image_size*image_size
middle_neurons = 100
delay = 1.0
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 20,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 1,      #The refractory period in ms
                   'tau_syn_E' : 1.25,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 15,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -50  #The threshold voltage at which the neuron will spike.
                   }

cell_params_lif1 = {'cm'        : 0.35, # nF #capacitance of LIF neuron in nF
                   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
                   'tau_m'     : 5,     #The time-constant of the RC circuit, in ms
                   'tau_refrac': 1,      #The refractory period in ms
                   'tau_syn_E' : 1,    #The excitatory input current decay time-constant
                   'tau_syn_I' : 15,    #The inhibitory input current decay time-constant
                   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
                   'v_rest'    : -65,  #The ambient rest voltage of the neuron
                   'v_thresh'  : -50  #The threshold voltage at which the neuron will spike.
                   }




timestep=1
p.setup(timestep=timestep)
################Load Stimulus Data #####################
ip_path= '/home/ruthvik/Desktop/DataSets/DoGdata/test_sep150/'
output=open(ip_path+'train_stimulus'+str(nofImages)+'_Images100_separationmanual_1_sigma2_k_typesON_spklmt100_digit'+str(digit)+'.pkl','rb')
allspikes = pickle.load(output)
output.close()
################Load weights ##########################
path= '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/500kImgs/'
output = open(path+'ON_post_weights_inh_20_150tstp500000Imgs.pkl','rb')
weights = pickle.load(output)
output.close()
### 3 types of populations defined ########
stimlus_pop = p.Population(ip_Neurons,p.SpikeSourceArray(spike_times= allspikes),label="driver")
middle_population =  p.Population(middle_neurons, p.IF_curr_exp,cell_params_lif, label='middle neurons')
inh_population = p.Population(middle_neurons, p.IF_curr_exp, cell_params_lif1,label="Inhibitory")

#####2 static synapse between IP and middle pop and ######
stat_exc_learned = p.StaticSynapse(delay=delay)
stat_inh = p.StaticSynapse(weight=-50, delay=delay/4.0)
stat_exc = p.StaticSynapse(weight=50, delay=delay/4.0)

#################Projections defined ######################
stimTomiddle = p.Projection(stimlus_pop,middle_population,p.FromListConnector(weights,safe = True,column_names=["weight"]),synapse_type = stat_exc_learned,receptor_type="excitatory")
#middleToinh = p.Projection(middle_population,inh_population,p.AllToAllConnector(allow_self_connections=False),synapse_type =stat_exc , receptor_type="excitatory")
#inhTomiddle = p.Projection(inh_population,middle_population,p.AllToAllConnector(allow_self_connections=False),synapse_type = stat_inh, receptor_type="inhibitory")

middle_population.record('spikes')
path= '/home/ruthvik/Desktop/Fullyconnected_Results/default_models/500kImgs/uninh_test/reset/'
class MyProgressBar(object):
    #"""
    #A callback which draws a progress bar in the terminal.
    #"""
    
    def __init__(self, interval, t_stop, population):
        self.interval = interval
        self.t_stop = t_stop
        self.pb = ProgressBar(width=100, char="#")
        self.population = population
        
    def __call__(self, t):
        if(t%image_duration==0):
            self.population.set_membrane_potential([(i,cell_params_lif['v_reset']) for i in \
                                                range(middle_neurons)])
        self.pb(t / self.t_stop)
        return t + self.interval


p.run(nofImages*image_duration,callbacks=[MyProgressBar(timestep,nofImages*image_duration,middle_population)])
####################################################################################
####################### Extracting and Saving Weights #############################
#post_weights = stimTomiddle.get('weight',format='list',with_address=True)
#picklefile2="test_post_weights"+str(nofImages)+"Imgs"+".pkl"
#output = open(path+picklefile2,'wb')
#pickle.dump(post_weights,output)
#output.close()
#print('after run weights pkl file saved at {}').format(path+picklefile2)


######################Plotting and Spiketrains of middle##########################
############################    layer  ##########################################
data = middle_population.get_data().segments[0]
spikes = data.spiketrains
picklefile3="train_op_spikes_ON"+str(nofImages)+"Imgs"+str(digit)+"_digit"+".pkl"
output = open(path+picklefile3,'wb')
pickle.dump(spikes,output)
output.close()
print('spikes are saved to {}').format(path+picklefile3)
p.end()
