"""The differenc between this code and fulyconnect.py is that here, only 1 of the output is let to fire and it will
update the weights similar to Nesslers paper, but this code runs very slow because of on the go making of the mask,
we will have 729 queries(to find if the pre syn neurons has fired between the start of the present image and the 
present timestep)into the allspikes dataset because of 729 pre synaptic neruons, each of this query takes 1ms so
totally it takes 700ms as a result, it's very slow, for updating the weights numpy mask is NOT used as in the 
previous version, this just updates weights in a for loop decisions to see if a presynaptic neuron has spiked 
between present tstep and the starting of the present image are made available here instead of calculating it on 
the go, which sped up the code. see pre_spikes/full_simple_boolspiketrain.py to see how the decisions are made
this code does kwta"""

import pyNN
import pyNN.neuron as p
import pickle
from pyNN.utility.plotting import Figure, Panel 
import progressbar
import sys
import numpy as np
import timeit
import h5py

image_size=27
nofImages= 60000
image_duration=60 ##Changes if DOG/POisson
A_plus = 0.004
A_minus = 0.003
ip_Neurons = image_size*image_size
op_Neurons = 100
datagrab_inter =5000 
kwta = 2
data_grab =[datagrab_inter*i for i in range(0,nofImages/datagrab_inter)]
data_grab= [items*image_duration for items in data_grab]
SAVE_DATA=not False
DEBUG =not True
delay = 1.0
cell_params_lif = {'cm'        : 1.0, # nF #capacitance of LIF neuron in nF
				   'i_offset'  : 0.0,      #A base input current to add each timestep.(What current??)
				   'tau_m'     : 60,     #The time-constant of the RC circuit, in ms
				   'tau_refrac': 1,      #The refractory period in ms
				   'tau_syn_E' : 2,    #The excitatory input current decay time-constant
				   'tau_syn_I' : 5,    #The inhibitory input current decay time-constant
				   'v_reset'   : -70,  #The voltage to set the neuron at immediately after a spike
				   'v_rest'    : -68,  #The ambient rest voltage of the neuron
				   'v_thresh'  : -30  #The threshold voltage at which the neuron will spike.
				   }
timestep=1
p.setup(timestep=timestep)
print('nofImages:{},A_plus:{},A_minus:{},timestep:{}').format(nofImages,A_plus,A_minus,timestep)

path = '/home/ruthvik/Desktop/DataSets/DoGdata/Freshdata/roundoff_data_cv2/train_60ts50st/'
output = open(path+'train_60000_Imgs10_mute_rndoff_1_sigma2_k_types_ON50_tstps.pkl','rb')
allspikes = pickle.load(output)
output.close()

ipfile = 'numpy_bool_prev_spks_60ts50st60000_Images_.h5'
with h5py.File(path+ipfile, 'r') as hf:
	booldata = hf['Spike_numpy_bool'][:]

t_stop = (nofImages+1)*image_duration
### 2 types of populations defined ########
ip_pop = p.Population(ip_Neurons,p.SpikeSourceArray(spike_times= allspikes),label="input neurons")
op_pop =  p.Population(op_Neurons, p.IF_curr_exp,cell_params_lif, label='output neurons')
### Random weights for stim_pop tp middle_pop ################
weights = pyNN.random.RandomDistribution('normal_clipped',[0.8,0.04,0.05,0.95])

#####################Synapses defined #####################3

stat_exc = p.StaticSynapse(weight=weights, delay=delay/4.0)

#################Projections defined ######################
ip2op = p.Projection(ip_pop,op_pop,p.AllToAllConnector(allow_self_connections=False),synapse_type = stat_exc, receptor_type="excitatory")

#################Saving weights of pre run ################
if(SAVE_DATA):
	path = '/home/ruthvik/Desktop/Fullyconnected_Results/v4_FCN/'
	pre_weights = ip2op.get('weight',format='list',with_address=True)
	picklefile1="weights_60ts50st"+str(nofImages)+"Imgs"+".pkl"
	output = open(path+picklefile1,'wb')
	pickle.dump(pre_weights,output)
	output.close()
	print('before run weights pkl file saved at {}').format(path+picklefile1)
op_pop.record('spikes','v')

bar = progressbar.ProgressBar(maxval=t_stop, \
	widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()

start_time = timeit.default_timer()

class WeightUpdate(object):
    def __init__(self, interval, population, projection):
        self.interval = interval
        self.population = population
        self.cell_voltages = []
        self.projection =projection
        self.spiketimes = [[] for i in range(ip_Neurons)]
    def __call__(self, t):
        if(t%image_duration==0):
            self.population.set_membrane_potential([(i,cell_params_lif['v_reset']) for i in range(op_Neurons)]) 
        time_bin = int(t)/image_duration
        self.cell_voltages=self.population.get_membrane_potential()[:]
		#self.cell_voltages.sort(reverse=True)
        neur_ids =np.argwhere(np.asarray(self.cell_voltages)>cell_params_lif['v_thresh']).flatten().tolist()
        if(len(neur_ids)!=0):
            if(DEBUG):
                print('Neurons that fired:{} at:{}\n').format(neur_ids,t)
            #if(len(neur_ids)>=kwta):
            #	updates = np.random.randint(low=kwta/2,high=kwta,size=1)[0]
            #else: 
            #	updates = np.random.randint(low=len(neur_ids)/2,high=len(neur_ids),size=1)[0]
            #neur_ids = neur_ids[:updates]
            updates = kwta
            neur_ids = np.argpartition(self.cell_voltages, -updates)[-updates:].tolist()#this lines gives you updatesi
            #number of highest values, this is  used.
            if(len(neur_ids)!=0):
                if(DEBUG):
                    print('Neurons:{} updated at:{}\n').format(neur_ids,t)
                all_weights =self.projection.get('weight',format='list',with_address=True)
                all_synapses = list(self.projection.connections)
                for neur_id in neur_ids:
                    self.spiketimes[neur_id].append(t)
                    weights = all_weights[(ip_Neurons*neur_id):(ip_Neurons*(neur_id+1))]	
                    weights = [weights[i][2] for i in range(len(weights))]
                    synapses = all_synapses[ip_Neurons*neur_id:ip_Neurons*(neur_id+1)]
                    for i in range(0,len(weights)):
                        Q = booldata[i][int(t)] 
                        if (Q):
                            weights[i]=weights[i]+A_plus*weights[i]*(1-weights[i])
                            synapses[i].weight=weights[i]
                        else:
                            weights[i]=weights[i]-A_minus*weights[i]*(1-weights[i])
                            synapses[i].weight=weights[i]
        if(SAVE_DATA):
            if(t in data_grab):
                post_weights = self.projection.get('weight',format='list',with_address=True)
                picklefile="weights_60ts50st"+str(time_bin)+"Imgs"+".pkl"
                output = open(path+picklefile,'wb')
                pickle.dump(post_weights,output)
                output.close()
                print('saved weights at image:{}').format(time_bin)


        bar.update(t+self.interval-1)
        #print 'time step',t
        return t + self.interval
	def get_spiketimes(self):
		return self.spiketimes
weight_update = WeightUpdate(timestep,op_pop,ip2op)
p.run(nofImages*image_duration,callbacks=[weight_update])
bar.finish()

spikes = weight_update.get_spiketimes()


elapsed=timeit.default_timer()-start_time
print 'elapsed time', elapsed
####################################################################################
####################### Extracting and Saving Weights #############################
if(SAVE_DATA):
	post_weights = ip2op.get('weight',format='list',with_address=True)
	picklefile2="weights_60ts50st"+str(nofImages)+"Imgs"+".pkl"
	output = open(path+picklefile2,'wb')
	pickle.dump(post_weights,output)
	output.close()
	print('after run weights pkl file saved at {}').format(path+picklefile2)

	picklefile3="ON_op_pop_spikes"+str(nofImages)+"Imgs"+".pkl"
	output = open(path+picklefile3,'wb')
	pickle.dump(spikes,output)
	output.close()
	print('spikes are saved to {}').format(path+picklefile3)

