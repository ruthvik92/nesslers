'''Taken from http://scikit-learn.org/stable/tutorial/basic/tutorial.html , data format should be (n_samples,n_features)...
here we have (50000,100) 50000 is number of images and 100 is dimensions of each image'''

import pickle
import numpy as np
from sklearn import svm
output = open('/home/ruthvik/Desktop/train_data_labels24500.pkl','rb')
train = pickle.load(output)
output.close()
clf = svm.SVC(gamma=0.001, C=100.)
train_labels = train[1]
trains = [np.array(train[0][i]) for i in range(0,len(train[0]))]
'''clf.fit(trains, train_labels)
correct=0
for i in range(5000):
    x = clf.predict(trains[i].reshape(1,-1))
    if(x[0]==train_labels[i]):
        correct+=1

print('training accuracy is {}').format(correct/5000.0)



#####Testing #########################
output = open('/home/ruthvik/Desktop/test_data_labels4500.pkl','rb')
test = pickle.load(output)
output.close()
test_labels = test[1]
tests = [np.array(test[0][i]) for i in range(0,len(test[0]))]
correct=0
for i in range(4500):
    x = clf.predict(tests[i].reshape(1,-1))
    if(x[0]==test_labels[i]):
        correct+=1
      
print('testing accuracy is {}').format(correct/4500.0)'''
