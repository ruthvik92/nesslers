"""This code does the ON and OFF center operations using DoG and it generates
spikes if a pixel value crosses a threshold and also it plots the spike train
the last image, it also reconstructs the image from the spikes by accumulating.
The pixel value is subtracted by threshold and all the pixel values that are less
less than or equal to 0 are set to zero and intensities are sorted from low to high
and then the pixel with the highest intensity is made to spike first and the next 
intensity  will spike after 1ms and so on..This code limits the number of spikes 
to tsteps, this code saves the ON and OFF center spieks as pkl files. This file
will generate spikes for separate digits, means all 0's are put in one spikefile
likewise all 1s, 2's etc, it will save both OFF center and ON center spikefiles"""
import pickle
import poisson_tools as pstool
import numpy as np
import scipy
import matplotlib.pyplot as plt
import progressbar
import pickle
from copy import deepcopy
from skimage import data, feature, color, filters, img_as_float
train_x, train_y = pstool.get_test_data() ##will call it train but it's actually test

#unique, counts = np.unique(test_y, return_counts=True) #used to get number of unique digits in an array
##Uncomment all #** to see if you're encoding correct digits. and change range in for digit in range(10)

separation = 100 #same as tsteps
sigma=1
k=2
thresh=20
tsteps=100
bar = progressbar.ProgressBar(maxval=10, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
for digit in range(10):
    labelval = digit
    labelindices= np.where(train_y==labelval)[0] ##Looking for all indices where this label exists.
    NoOfImages=len(labelindices)
    offallspikes = [[] for i in range(0,27*27)]
    onallspikes = [[] for i in range(0,27*27)]
    #**plt.figure(0)
    ########################################################################
    #####################This part is where ################################
    ##################### you reshape the image#############################
    ####################the to 27x27 and do ################################
    ####################gaussian filter with################################
    ####################given k and sigma###################################
    for v in range(len(labelindices)):
      img = train_x[labelindices[v]:labelindices[v]+1]
      de0 = [ i for i in range(0,28)]
      de = [28*i-1 for i in range(2,29)]
      de0.extend(de)
      new_img = np.delete(img,de0)
      new_img = new_img.reshape(1,729)
      img = np.reshape(new_img,(27,27))
      img = color.rgb2gray(img)
      #**plt.subplot(1,5,1)
      #**im = plt.imshow(img)
      #**plt.title('Original Image')
      s1 = filters.gaussian(img,k*sigma)
      s2 = filters.gaussian(img,sigma)
    #######################################################################
    #####################This part is where ###############################
    ##################### you calc OFF center #############################

      dog = s1 - s2
      #**plt.subplot(1,5,2)
      #**print dog.min(),dog.max()
      #**plt.imshow(dog,cmap='gray')
      #**plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))

    #######################################################################
    #####################This part is where ###############################
    ##################### you calc spiketrains ############################
    ##################### of OFF center filter ############################

      #**print(sum(sum(dog>thresh)))
      dog=dog-thresh
      nofspikes =sum(sum(dog>0))
      offspikepos = np.argwhere(dog>0)
      dog[dog<=0]=0
      offspiketimes=[[] for i in range(27*27)]
      for items in offspikepos:
        x=dog[items[0]][items[1]]
        offspiketimes[27*(items[0])+(items[1])].append(x)
          
    #######################################################################
    #####################This part is where ###############################
    ##################### you comprss or expand ###########################
    ##################### spiketrains to suit tstep #######################
      newoffspiketimes = [[] for i in range(27*27)]
      list_offspiketimes = [items[0] if len(items)!=0 else None for items in offspiketimes]
      unsortlst_offspiketimes = deepcopy(list_offspiketimes)
      list_offspiketimes.sort()
      
      for t in range(tsteps):
        qa = unsortlst_offspiketimes.index(list_offspiketimes[-t])
        if(qa!=0):
          newoffspiketimes[qa].append(t+tsteps*v)

    ########Appending all OFF center spikes ##############################
      for i in range(0,len(offallspikes)):
        offallspikes[i].extend(newoffspiketimes[i])
        
    #######################################################################
    #####################This part is where ###############################
    ##################### you reconst OFF center ##########################
      #**print('No of Neurons(OFF) in each row greater than {} is {}').format(thresh,nofspikes)
      offspiketimes = newoffspiketimes ##Replce with offspiketimes to see 
      #**plt.subplot(1,5,3)               #not modified spiketrains
      recon_img = [1]*729
      for i in range(0,len(offspiketimes)):
        if(len(offspiketimes[i])!=0):
          recon_img[i]=0
      recon_img=np.array(recon_img).reshape(27,27)
      #**plt.imshow(recon_img,cmap='gray')
      #**plt.title('Reconst of OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))


    #######################################################################
    #####################This part is where ###############################
    ##################### you calc ON center ##############################
      dog1 = s2-s1
      #**plt.subplot(1,5,4)
      #**print dog1.min(),dog1.max()
      #**plt.imshow(dog1,cmap='gray')
      #**plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k))
      
    #######################################################################
    #####################This part is where ###############################
    ##################### you calc spiketrains ############################
    ##################### of ON center filter #############################
      #**print(sum(sum(dog1>thresh)))
      dog1 = dog1-thresh
      nofspikes =sum(sum(dog1>0))
      onspikepos = np.argwhere(dog1>0)
      dog[dog<=1]=0
      onspiketimes=[[] for i in range(27*27)]
      for items in onspikepos:
        y=dog1[items[0]][items[1]]
        onspiketimes[27*(items[0])+(items[1])].append(y)
          
    #######################################################################
    #####################This part is where ###############################
    ##################### you comprss or expand ###########################
    ##################### spiketrains to suit tstep #######################
      newonspiketimes = [[] for i in range(27*27)]
      list_onspiketimes = [items[0] if len(items)!=0 else None for items in onspiketimes]
      unsortlst_onspiketimes = deepcopy(list_onspiketimes)
      list_onspiketimes.sort()

      for t in range(tsteps):
        qa = unsortlst_onspiketimes.index(list_onspiketimes[-t])
        if(qa!=0):
          newonspiketimes[qa].append(t+tsteps*v)

    ########Appending all ON center spikes ###############################
      for i in range(0,len(onallspikes)):
        onallspikes[i].extend(newonspiketimes[i])
        
    #######################################################################
    #####################This part is where ###############################
    ##################### you reconst ON center ###########################
      #**print('No of Neurons(ON) in each row greater than {} is {}').format(thresh,nofspikes)
      onspiketimes=newonspiketimes
      #**plt.subplot(1,5,5)
      recon_img = [0]*729
      for i in range(0,len(onspiketimes)):
        if(len(onspiketimes[i])!=0):
          recon_img[i]=1
      recon_img=np.array(recon_img).reshape(27,27)
      #**plt.imshow(recon_img,cmap='gray')
      #**plt.title('Reconst of ON DoG with sigma=' + str(sigma) + ', k=' + str(k))

      #**plt.show() 
      
    #**plt.figure(1)
    pstool.raster_plot_spike(onallspikes,'*')
    plt.show()

    plt.figure(2)
    pstool.raster_plot_spike(offallspikes,'^')
    plt.show()

    path = '/home/ruthvik/DoGdata/test_sep/'

    picklefile1="train_stimulus"+str(NoOfImages)+'_Images'+str(separation)+"_separation"+"manual_"+ str(sigma)+"_sigma"+str(k)+"_k_types"+"ON"+"_spklmt"+str(separation)+"_digit"+str(digit)+".pkl"
    output1 = open(path+picklefile1,'wb')
    pickle.dump(onallspikes,output1)
    output1.close()
    print "pickle file written to",path+picklefile1

    picklefile2="train_stimulus"+str(NoOfImages)+'_Images'+str(separation)+"_separation"+"manual_"+ str(sigma)+"_sigma"+str(k)+"_k_types"+"OFF"+"_spklmt"+str(separation)+"_digit"+str(digit)+".pkl"
    output2 = open(path+picklefile2,'wb')
    pickle.dump(offallspikes,output2)
    output2.close()
    print "pickle file written to",path+picklefile2
    bar.update(digit) 

bar.finish()




