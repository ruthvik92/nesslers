"""This code does the ON and OFF center operations using DoG and it generates
spikes if a pixel value crosses a threshold and also it plots the spike train
the last image, it also reconstructs the image from the spikes by making pixels
whose strength is proportional to the time of the spike.
The pixel value is subtracted by threshold and all the pixel values that are less
less than or equal to 0 are set to zero and intensities are sorted from low to high
and then the pixel with the highest intensity is made to spike first and the next 
intensity  will spike after 1ms and so on..This code limits the number of spikes to tsteps"""
import scipy
import numpy as np
import matplotlib.pyplot as plt
import progressbar
from copy import deepcopy
from skimage import data, feature, color, filters, img_as_float
import poisson_tools as pstool
from sklearn.preprocessing import normalize
train_x,train_y = pstool.get_train_data()

sigma=1
k=2
thresh=20
tsteps=75   ##No of spikes per image
NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
allspikes = [[] for i in range(0,27*27)]
prev_max = 0
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()

########################################################################
#####################This part is where ################################
##################### you reshape the image#############################
####################the to 27x27 and do ################################
####################gaussian filter with################################
####################given k and sigma###################################
for v in range(0,NoOfImages):
  img = train_x[v:v+1]
  de0 = [ i for i in range(0,28)]
  de = [28*i-1 for i in range(2,29)]
  de0.extend(de)
  new_img = np.delete(img,de0)
  new_img = new_img.reshape(1,729)
  img = np.reshape(new_img,(27,27))
  img = color.rgb2gray(img)
  plt.subplot(1,5,1)
  im = plt.imshow(img)
  plt.title('Original Image')
  s1 = filters.gaussian(img,k*sigma)
  s2 = filters.gaussian(img,sigma)
#######################################################################
#####################This part is where ###############################
##################### you calc OFF center #############################

  dog = s1 - s2
  plt.subplot(1,5,2)
  print dog.min(),dog.max()
  plt.imshow(dog,cmap='gray')
  plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))

#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of OFF center filter ############################

  print(sum(sum(dog>thresh)))
  dog=dog-thresh
  nofspikes =sum(sum(dog>0))
  offspikepos = np.argwhere(dog>0) ##corrdinates whose value >0
  dog[dog<=0]=0
  offspiketimes=[[] for i in range(27*27)]
  for items in offspikepos:
    x=dog[items[0]][items[1]]
    offspiketimes[27*(items[0])+(items[1])].append(x)##27*items[0]+items[1]
    #gives neuron number
      
#######################################################################
#####################This part is where ###############################
##################### you comprss or expand ###########################
##################### spiketrains to suit tstep #######################
      
  newoffspiketimes = [[] for i in range(27*27)]
  list_offspiketimes = [items[0] if len(items)!=0 else None for items in offspiketimes]
  unsortlst_offspiketimes = deepcopy(list_offspiketimes)
  list_offspiketimes.sort()
  
  for t in range(tsteps):
    qa = unsortlst_offspiketimes.index(list_offspiketimes[-t])
    if(qa!=0):
      newoffspiketimes[qa].append(t) ##To prevent the neuron containing
                                     ##None
  #newoffspiketimes=offspiketime
#######################################################################
#####################This part is where ###############################
##################### you reconst OFF center ##########################
  print('No of Neurons(OFF) in each row greater than {} is {}').format(thresh,nofspikes)
  offspiketimes = newoffspiketimes ##Replce with offspiketimes to reconstrct 
  plt.subplot(1,5,3)               #not modified spiketrains
  recon_img = [0]*729
  for i in range(0,len(offspiketimes)):
    if(len(offspiketimes[i])!=0):
      #recon_img[i]=0     ##This will just accumulate the spikes without regard to strength of pixel encoded in time of arrival
      recon_img[i]=offspiketimes[i][0]  # Since we're reconst'ing the image with 0s, the lower the time,lighter the pixel
  recon_img=np.array(recon_img).reshape(27,27) #you don't do the inverse cos, we didn't invert before
  plt.imshow(recon_img,cmap='gray')
  plt.title('Reconst of OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))


#######################################################################
#####################This part is where ###############################
##################### you calc ON center ##############################
  dog1 = s2-s1
  plt.subplot(1,5,4)
  print dog1.min(),dog1.max()
  plt.imshow(dog1,cmap='gray')
  plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k))
  
#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of ON center filter #############################
  print(sum(sum(dog1>thresh)))
  dog1 = dog1-thresh
  nofspikes =sum(sum(dog1>0))
  onspikepos = np.argwhere(dog1>0)
  dog1[dog1<=0]=0
  onspiketimes=[[] for i in range(27*27)]
  for items in onspikepos:
    y=dog1[items[0]][items[1]]
    onspiketimes[27*(items[0])+(items[1])].append(y)
      
#######################################################################
#####################This part is where ###############################
##################### you comprss or expand ###########################
##################### spiketrains to suit tstep #######################
  newonspiketimes = [[] for i in range(27*27)]
  list_onspiketimes = [items[0] if len(items)!=0 else None for items in onspiketimes]
  unsortlst_onspiketimes = deepcopy(list_onspiketimes)
  list_onspiketimes.sort()

  for t in range(tsteps):
    qa = unsortlst_onspiketimes.index(list_onspiketimes[-t])
    if(qa!=0):
      newonspiketimes[qa].append(t)

  #newonspiketimes=onspiketimes
#######################################################################
#####################This part is where ###############################
##################### you reconst ON center ###########################
  print('No of Neurons(ON) in each row greater than {} is {}').format(thresh,nofspikes)
  onspiketimes=newonspiketimes
  plt.subplot(1,5,5)
  recon_img = [0]*729
  for i in range(0,len(onspiketimes)):
    if(len(onspiketimes[i])!=0):
      #recon_img[i]=1
      recon_img[i]=onspiketimes[i][0] 
  recon_img=np.array(recon_img).reshape(27,27)
  plt.imshow(recon_img,cmap='gray')
  plt.title('Reconst of ON DoG with sigma=' + str(sigma) + ', k=' + str(k))

  plt.show() 
  bar.update(v) 

bar.finish()
pstool.raster_plot_spike(newonspiketimes,'*')
pstool.raster_plot_spike(newoffspiketimes,'^')
plt.show()

