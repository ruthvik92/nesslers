"""This code does the ON and OFF center operations using DoG and it generates
spikes if a pixel value crosses a threshold and also it plots the spike train
the last image, it also reconstructs the image from the spikes by accumulating.
The pixel value is subtracted by threshold and inverted and scaled with tsteps
to make sure that an image lies within  tsteps number of timesteps and also
in this code all the spiketimes are rounded off to the nearest integer, this
ode saves the ON and OFF center spikes as pickle values, added
on and off multiplication factors. edit:When you inver the pixel intensities, the
resultant time values will be very small(greater than 0 but very less than 1)
so, these multiplication factors will magnify those small numbers, in a way
this will pack the closes events together."""
import scipy
import numpy as np
import matplotlib.pyplot as plt
import progressbar
import cv2
import pickle
from skimage import data, feature, color, filters, img_as_float
import poisson_tools as pstool
train_x,train_y = pstool.get_train_data()
reconst = False
rasterplots = False
savefiles= True
save_sparse_arrays = True ## this is a time series of sparsee arrays.
separation = 10
image_size=27
sigma=1
k=2
thresh=10#21.5
on_multip_factor = 500#250#750#1000#1500
off_multip_factor=125
tsteps=30
NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
allspikes = [[] for i in range(0,27*27)]
prev_max = 0
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
offallspikes = [[] for i in range(0,27*27)]
onallspikes = [[] for i in range(0,27*27)]
sparse_array_on = np.zeros((image_size,image_size,(tsteps+separation)*NoOfImages),dtype ='uint32')
sparse_array_off = np.zeros((image_size,image_size,(tsteps+separation)*NoOfImages),dtype ='uint32')
on_spikes_per_digit={}
for dig in range(10):
  on_spikes_per_digit[dig]=0

off_spikes_per_digit={}
for dig in range(10):
  off_spikes_per_digit[dig]=0
if(reconst):
  plt.figure(0)
########################################################################
#####################This part is where ################################
##################### you reshape the image#############################
####################the to 27x27 and do ################################
####################gaussian filter with################################
####################given k and sigma###################################
for v in range(0,NoOfImages):
  img = train_x[v:v+1]
  de0 = [ i for i in range(0,28)]
  de = [28*i-1 for i in range(2,29)]
  de0.extend(de)
  new_img = np.delete(img,de0)
  new_img = new_img.reshape(1,729)
  img = np.reshape(new_img,(27,27))
  img = color.rgb2gray(img)
  if(reconst):
    plt.subplot(1,5,1)
    im = plt.imshow(img,cmap='gray')
    ax=plt.gca()
    ax.set_xticks(np.arange(0, 27, 7));
    ax.set_yticks(np.arange(0, 27, 7));
    ax.grid(which='major', axis='both', linestyle='-', linewidth=2)
    #plt.title('Original Image',fontsize=38)
  #s1 = filters.gaussian(img,k*sigma)
    #if use this method, filter size is determined by 6*sigma-1
    #see https://en.wikipedia.org/wiki/Gaussian_filter for further info
  #s2 = filters.gaussian(img,sigma)
  s1 = cv2.GaussianBlur(img,(7,7),k*sigma)
  s2 = cv2.GaussianBlur(img,(7,7),sigma)
#######################################################################
#####################This part is where ###############################
##################### you calc OFF center #############################

  dog = s1 - s2
  if(reconst):
    plt.subplot(1,5,2)
    print dog.min(),dog.max()
    ax=plt.gca()
    ax.set_xticks(np.arange(0, 27, 7));
    ax.set_yticks(np.arange(0, 27, 7));
    ax.grid(which='major', axis='both', linestyle='-', linewidth=2)
    plt.imshow(dog,cmap='gray',interpolation='none')
    #plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k),fontsize=38)

#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of OFF center filter ############################
  if(reconst):
    print(sum(sum(dog>thresh)))
  dog=dog-thresh
  nofspikes =0#=sum(sum(dog>0))
  offspikepos = np.argwhere(dog>0)
  dog[dog<=0]=0
  offspiketimes=[[] for i in range(27*27)]
  for items in offspikepos:
    x=(1./abs(dog[items[0]][items[1]]))*off_multip_factor
    if(x<=tsteps):
      nofspikes+=1
      offspiketimes[27*(items[0])+(items[1])].append(x+(separation+tsteps)*v)
      sparse_array_off[int(items[0]),int(items[1]),int(np.round(x))]=1
      
#######################################################################
#####################This part is where ###############################
##################### you comprss or expand ###########################
##################### spiketrains to suit tstep #######################
  offspiketimes_rndarray = []
  for items in offspiketimes:
    if(len(items)!=0):
      zx=items[0]
      #zx=items[0]+separation*(v+1)
      offspiketimes_rndarray.append(np.round([zx]))
    else:
        offspiketimes_rndarray.append(np.round([]))
  newoffspiketimes = [items.tolist() for items in offspiketimes_rndarray]

########Appending all OFF center spikes ##############################
  for i in range(0,len(offallspikes)):
    offallspikes[i].extend(newoffspiketimes[i])
    
#######################################################################
#####################This part is where ###############################
##################### you reconst OFF center ##########################
  off_spikes_per_digit[train_y[v]]+=nofspikes
  if(reconst):
    print('No of Neurons(OFF) in each row greater than {} is {}').format(thresh,nofspikes)
  offspiketimes = newoffspiketimes ##Replce with offspiketimes to see 
  if(reconst):
    plt.subplot(1,5,3)               #not modified spiketrains
  recon_img = [0]*729
  for i in range(0,len(offspiketimes)):
    if(len(offspiketimes[i])!=0):
      recon_img[i]=255*(1.0/offspiketimes[i][0])
  recon_img=np.array(recon_img).reshape(27,27)
  if(reconst):
    ax=plt.gca()
    ax.set_xticks(np.arange(0, 27, 7));
    ax.set_yticks(np.arange(0, 27, 7));
    ax.grid(which='major', axis='both', linestyle='-', linewidth=2)
    plt.imshow(recon_img,cmap='gray',interpolation='none')
    #plt.title('Reconst of OFF DoG with sigma=' + str(sigma) + ', k=' + str(k),fontsize=38)


#######################################################################
#####################This part is where ###############################
##################### you calc ON center ##############################
  dog1 = s2-s1
  if(reconst):
    plt.subplot(1,5,4)
    print dog1.min(),dog1.max()
    ax=plt.gca()
    ax.set_xticks(np.arange(0, 27, 7));
    ax.set_yticks(np.arange(0, 27, 7));
    ax.grid(which='major', axis='both', linestyle='-', linewidth=2)
    plt.imshow(dog1,cmap='gray',interpolation='none')
    #plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k),fontsize=38)
  
#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of ON center filter #############################
  if(reconst):
    print(sum(sum(dog1>thresh)))
  dog1 = dog1-thresh
  nofspikes =0#sum(sum(dog1>0))
  onspikepos = np.argwhere(dog1>0)
  dog[dog<=0]=0
  onspiketimes=[[] for i in range(27*27)]
  for items in onspikepos:
    y=(1./dog1[items[0]][items[1]])*on_multip_factor
    if(y<=tsteps):
      nofspikes+=1
      onspiketimes[27*(items[0])+(items[1])].append(y+(separation+tsteps)*v)
      sparse_array_on[int(items[0]),int(items[1]),int(np.round(y))]=1
      
#######################################################################
#####################This part is where ###############################
##################### you comprss or expand ###########################
##################### spiketrains to suit tstep #######################
  onspiketimes_rndarray = []
  for items in onspiketimes:
    if(len(items)!=0):
      zx=items[0]
      #zx=items[0]+separation*(v+1)
      onspiketimes_rndarray.append(np.round([zx]))
    else:
      onspiketimes_rndarray.append(np.round([]))
  newonspiketimes = [items.tolist() for items in onspiketimes_rndarray]

########Appending all ON center spikes ###############################
  for i in range(0,len(onallspikes)):
    onallspikes[i].extend(newonspiketimes[i])
    
#######################################################################
#####################This part is where ###############################
##################### you reconst ON center ###########################
  on_spikes_per_digit[train_y[v]]+=nofspikes
  if(reconst):
    print('No of Neurons(ON) in each row greater than {} is {}').format(thresh,nofspikes)
  onspiketimes=newonspiketimes
  if(reconst):
    plt.subplot(1,5,5)
  recon_img = [0]*729
  for i in range(0,len(onspiketimes)):
    if(len(onspiketimes[i])!=0):
      recon_img[i]=255*(1.0/onspiketimes[i][0])
  recon_img=np.array(recon_img).reshape(27,27)
  if(reconst):
    ax=plt.gca()
    ax.set_xticks(np.arange(0, 27, 7));
    ax.set_yticks(np.arange(0, 27, 7));
    ax.grid(which='major', axis='both', linestyle='-', linewidth=2,color='w')
    plt.imshow(recon_img,cmap='gray',interpolation='none')
    #plt.title('Reconst of ON DoG with sigma=' + str(sigma) + ', k=' + str(k),fontsize=38)
  if(reconst):
    plt.suptitle('Original Image, OFF DoG, OFF DoG Reconstruction, ON DoG, ON DoG Reconstruction',fontsize=38)
    plt.show() 
  bar.update(v) 

bar.finish()
spikes_per_digit=[on_spikes_per_digit,off_spikes_per_digit]

if(reconst):
  plt.figure(1)
if(rasterplots):
  pstool.raster_plot_spike(onallspikes,'*')
  plt.xlabel('Time(ms)',fontsize=38)
  plt.ylabel('Pixel',fontsize=38)
  plt.tick_params(axis='x',labelsize=30)
  plt.tick_params(axis='y',labelsize=30)
  plt.show()

  plt.figure(2)
  pstool.raster_plot_spike(offallspikes,'^')
  plt.show()

  fig, axes = plt.subplots(1, 2,figsize=(16, 16))
  fig.subplots_adjust(hspace=0.2, wspace=0.2)
  ax = axes.flat
  for i in range(1,len(ax)+1):
    bars = ax[i-1].bar(range(len(spikes_per_digit[i-1])), spikes_per_digit[i-1].values(),tick_label=spikes_per_digit[i-1].keys(),align='center')
    reds = sorted(range(len(spikes_per_digit[i-1].values())), key=lambda po: spikes_per_digit[i-1].values()[po])[-5:]
    for red in reds:
      bars[red].set_color('r')
    reds.sort()
    #print reds
    ax[i-1].set_title('Dominant Digits:'+str(reds),fontsize=12)
    ax[i-1].set_ylim(0,1.5*(spikes_per_digit[i-1][reds[-1]]))
  plt.suptitle('Bar chart of number of spikes per digit in ON and OFF center.')
  plt.show()
if(savefiles):
  picklefile1="train_"+str(NoOfImages)+'_Imgs'+str(separation)+"_mute"+"_rndoff_"+ str(sigma)+"_sigma"+str(k)+"_k_types_"+"ON"+str(tsteps)+"_tstps"+".pkl"
  path = '/home/ruthvik/Desktop/testdata/'
  output1 = open(path+picklefile1,'wb')
  pickle.dump(onallspikes,output1)
  output1.close()
  print "pickle file written to",path+picklefile1

  picklefile2="train_"+str(NoOfImages)+'_Imgs'+str(separation)+"_mute"+"_rndoff_"+ str(sigma)+"_sigma"+str(k)+"_k_types_"+"OFF"+str(tsteps)+"_tstps"+".pkl"
  output2 = open(path+picklefile2,'wb')
  pickle.dump(offallspikes,output2)
  output2.close()
  print "pickle file written to",path+picklefile2
  
  picklefile3="train_sparse_on_array"+str(NoOfImages)+'_Imgs'+str(separation)+"_mute"+"_rndoff_"+ str(sigma)+\
  "_sigma"+str(k)+"_k_types_"+"OFF"+str(tsteps)+"_tstps"+".pkl"
  output3 = open(path+picklefile3,'wb')
  pickle.dump(sparse_array_on,output3)
  output3.close()
  print "pickle file written to",path+picklefile3
  

