"""This code does the ON and OFF center operations using DoG and it generates
spikes if a pixel value crosses a threshold and also it plots the spike train
the last image, it also reconstructs the image from the spikes by accumulating.
The pixel value is subtracted by threshold and inverted and scaled with tsteps
to make sure that an image lies within  tsteps number of timesteps and also
in this code all the spiketimes are rounded off to the nearest integer, this
ode saves the ON and OFF center spikes as pickle values"""
import scipy
import numpy as np
import matplotlib.pyplot as plt
import progressbar
import pickle
from skimage import data, feature, color, filters, img_as_float
import poisson_tools as pstool
train_x,train_y = pstool.get_train_data()

separation = 50 ##should be greater than tsteps

sigma=1
k=2
thresh=21.5
tsteps=30
NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
allspikes = [[] for i in range(0,27*27)]
prev_max = 0
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
offallspikes = [[] for i in range(0,27*27)]
onallspikes = [[] for i in range(0,27*27)]
#**plt.figure(0)
########################################################################
#####################This part is where ################################
##################### you reshape the image#############################
####################the to 27x27 and do ################################
####################gaussian filter with################################
####################given k and sigma###################################
for v in range(0,NoOfImages):
  img = train_x[v:v+1]
  de0 = [ i for i in range(0,28)]
  de = [28*i-1 for i in range(2,29)]
  de0.extend(de)
  new_img = np.delete(img,de0)
  new_img = new_img.reshape(1,729)
  img = np.reshape(new_img,(27,27))
  img = color.rgb2gray(img)
  #**plt.subplot(1,5,1)
  #**im = plt.imshow(img)
  #**plt.title('Original Image')
  s1 = filters.gaussian(img,k*sigma)
  s2 = filters.gaussian(img,sigma)
#######################################################################
#####################This part is where ###############################
##################### you calc OFF center #############################

  dog = s1 - s2
  #**plt.subplot(1,5,2)
  #**print dog.min(),dog.max()
  #**plt.imshow(dog,cmap='gray')
  #**plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))

#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of OFF center filter ############################

  #**print(sum(sum(dog>thresh)))
  dog=dog-thresh
  nofspikes =sum(sum(dog>0))
  offspikepos = np.argwhere(dog>0)
  dog[dog<=0]=0
  offspiketimes=[[] for i in range(27*27)]
  for items in offspikepos:
    x=1./abs(dog[items[0]][items[1]])
    if(x<=1):
      offspiketimes[27*(items[0])+(items[1])].append(separation*v+x*tsteps) ##it will reinforce the reconstructed image cos of offset
      
#######################################################################
#####################This version of code ###############################
##################### no changes to original ###########################
##################### data are made          #######################
  newoffspiketimes = offspiketimes
########Appending all OFF center spikes ##############################
  for i in range(0,len(offallspikes)):
    offallspikes[i].extend(newoffspiketimes[i])
    
#######################################################################
#####################This part is where ###############################
##################### you reconst OFF center ##########################
  #**print('No of Neurons(OFF) in each row greater than {} is {}').format(thresh,nofspikes)
  offspiketimes = newoffspiketimes ##Replce with offspiketimes to see 
  #**plt.subplot(1,5,3)               #not modified spiketrains
  recon_img = [0]*729
  for i in range(0,len(offspiketimes)):
    if(len(offspiketimes[i])!=0):
      recon_img[i]=1./offspiketimes[i][0]
  recon_img=np.array(recon_img).reshape(27,27)
  #**plt.imshow(recon_img,cmap='gray')
  #**plt.title('Reconst of OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))


#######################################################################
#####################This part is where ###############################
##################### you calc ON center ##############################
  dog1 = s2-s1
  #**plt.subplot(1,5,4)
  #**print dog1.min(),dog1.max()
  #**plt.imshow(dog1,cmap='gray')
  #**plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k))
  
#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of ON center filter #############################
  #**print(sum(sum(dog1>thresh)))
  dog1 = dog1-thresh
  nofspikes =sum(sum(dog1>0))
  onspikepos = np.argwhere(dog1>0)
  dog[dog<=0]=0
  onspiketimes=[[] for i in range(27*27)]
  for items in onspikepos:
    y=1./dog1[items[0]][items[1]]
    if(y<=1):
      onspiketimes[27*(items[0])+(items[1])].append(separation*v+y*tsteps)
      
#######################################################################
#######################################################################
#####################This version of code ###############################
##################### no changes to original ###########################
##################### data are made          #######################
  newonspiketimes = onspiketimes
########Appending all ON center spikes ###############################
  for i in range(0,len(onallspikes)):
    onallspikes[i].extend(newonspiketimes[i])
    
#######################################################################
#####################This part is where ###############################
##################### you reconst ON center ###########################
  #**print('No of Neurons(ON) in each row greater than {} is {}').format(thresh,nofspikes)
  onspiketimes=newonspiketimes
  #**plt.subplot(1,5,5)
  recon_img = [0]*729
  for i in range(0,len(onspiketimes)):
    if(len(onspiketimes[i])!=0):
      recon_img[i]=1./onspiketimes[i][0]
  recon_img=np.array(recon_img).reshape(27,27)
  #**plt.imshow(recon_img,cmap='gray')
  #**plt.title('Reconst of ON DoG with sigma=' + str(sigma) + ', k=' + str(k))

  #**plt.show() 
  bar.update(v) 

bar.finish()
#**plt.figure(1)
pstool.raster_plot_spike(onallspikes,'*')
plt.show()

plt.figure(2)
pstool.raster_plot_spike(offallspikes,'^')
plt.show()

picklefile1="train_stimulus_orig"+str(NoOfImages)+'_Images'+str(tsteps)+"_separation"+"manual_"+ str(sigma)+"_sigma"+str(k)+"_k_types"+"ON"+"_spklimit"+".pkl"
path = '/home/ruthvik/DoGdata/'
output1 = open(path+picklefile1,'wb')
pickle.dump(onallspikes,output1)
output1.close()
print "pickle file written to",path+picklefile1

picklefile2="train_stimulus_orig"+str(NoOfImages)+'_Images'+str(tsteps)+"_separation"+"manual_"+ str(sigma)+"_sigma"+str(k)+"_k_types"+"OFF"+"_spklimit"+".pkl"
output2 = open(path+picklefile2,'wb')
pickle.dump(offallspikes,output2)
output2.close()
print "pickle file written to",path+picklefile2

