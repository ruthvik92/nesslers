"""This code does the ON and OFF center operations using DoG and it generates
spikes if a pixel value crosses a threshold and also it plots the spike train
the last image, it also reconstructs the image from the spikes by accumulating.
The pixel value is subtracted by threshold and all the pixel values that are less
less than or equal to 0 are set to zero and intensities are sorted from low to high
and then the pixel with the highest intensity is made to spike first and the next 
intensity  will spike after 1ms and so on..This code limits the number of spikes 
to tsteps, this code saves the ON and OFF center spieks as pkl files."""
import scipy
import numpy as np
import matplotlib.pyplot as plt
import progressbar
import pickle
from copy import deepcopy
from skimage import data, feature, color, filters, img_as_float
import poisson_tools as pstool
train_x,train_y = pstool.get_test_data() ##CHANGE TO TEST IF YOU WANT TEST DATA ELSE KEEP IT TRAIN

separation = 100 #should be at least tsteps
sigma=1
k=2
thresh=20
tsteps=75
NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
offallspikes = [[] for i in range(0,27*27)]
onallspikes = [[] for i in range(0,27*27)]
#**plt.figure(0)
########################################################################
#####################This part is where ################################
##################### you reshape the image#############################
####################the to 27x27 and do ################################
####################gaussian filter with################################
####################given k and sigma###################################
for v in range(0,NoOfImages):
  img = train_x[v:v+1]
  de0 = [ i for i in range(0,28)]
  de = [28*i-1 for i in range(2,29)]
  de0.extend(de)
  new_img = np.delete(img,de0)
  new_img = new_img.reshape(1,729)
  img = np.reshape(new_img,(27,27))
  img = color.rgb2gray(img)
  #**plt.subplot(1,5,1)
  #**im = plt.imshow(img)
  #**plt.title('Original Image')
  s1 = filters.gaussian(img,k*sigma)
  s2 = filters.gaussian(img,sigma)
#######################################################################
#####################This part is where ###############################
##################### you calc OFF center #############################

  dog = s1 - s2
  #**plt.subplot(1,5,2)
  #**print dog.min(),dog.max()
  #**plt.imshow(dog,cmap='gray')
  #**plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))

#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of OFF center filter ############################

  #**print(sum(sum(dog>thresh)))
  dog=dog-thresh
  nofspikes =sum(sum(dog>0))
  offspikepos = np.argwhere(dog>0)
  dog[dog<=0]=0
  offspiketimes=[[] for i in range(27*27)]
  for items in offspikepos:
    x=dog[items[0]][items[1]]
    offspiketimes[27*(items[0])+(items[1])].append(x)
      
#######################################################################
#####################This part is where ###############################
##################### you comprss or expand ###########################
##################### spiketrains to suit tstep #######################
  newoffspiketimes = [[] for i in range(27*27)]
  list_offspiketimes = [items[0] if len(items)!=0 else None for items in offspiketimes]
  unsortlst_offspiketimes = deepcopy(list_offspiketimes)
  list_offspiketimes.sort()
  
  for t in range(tsteps):
    qa = unsortlst_offspiketimes.index(list_offspiketimes[-t])
    if(qa!=0):
      newoffspiketimes[qa].append(t+separation*v)

########Appending all OFF center spikes ##############################
  for i in range(0,len(offallspikes)):
    offallspikes[i].extend(newoffspiketimes[i])
    
#######################################################################
#####################This part is where ###############################
##################### you reconst OFF center ##########################
  #**print('No of Neurons(OFF) in each row greater than {} is {}').format(thresh,nofspikes)
  offspiketimes = newoffspiketimes ##Replce with offspiketimes to see 
  #**plt.subplot(1,5,3)               #not modified spiketrains
  recon_img = [1]*729
  for i in range(0,len(offspiketimes)):
    if(len(offspiketimes[i])!=0):
      recon_img[i]=0
  recon_img=np.array(recon_img).reshape(27,27)
  #**plt.imshow(recon_img,cmap='gray')
  #**plt.title('Reconst of OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))


#######################################################################
#####################This part is where ###############################
##################### you calc ON center ##############################
  dog1 = s2-s1
  #**plt.subplot(1,5,4)
  #**print dog1.min(),dog1.max()
  #**plt.imshow(dog1,cmap='gray')
  #**plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k))
  
#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of ON center filter #############################
  #**print(sum(sum(dog1>thresh)))
  dog1 = dog1-thresh
  nofspikes =sum(sum(dog1>0))
  onspikepos = np.argwhere(dog1>0)
  dog[dog<=1]=0
  onspiketimes=[[] for i in range(27*27)]
  for items in onspikepos:
    y=dog1[items[0]][items[1]]
    onspiketimes[27*(items[0])+(items[1])].append(y)
      
#######################################################################
#####################This part is where ###############################
##################### you comprss or expand ###########################
##################### spiketrains to suit tstep #######################
  newonspiketimes = [[] for i in range(27*27)]
  list_onspiketimes = [items[0] if len(items)!=0 else None for items in onspiketimes]
  unsortlst_onspiketimes = deepcopy(list_onspiketimes)
  list_onspiketimes.sort()

  for t in range(tsteps):
    qa = unsortlst_onspiketimes.index(list_onspiketimes[-t])
    if(qa!=0):
      newonspiketimes[qa].append(t+separation*v)

########Appending all ON center spikes ###############################
  for i in range(0,len(onallspikes)):
    onallspikes[i].extend(newonspiketimes[i])
    
#######################################################################
#####################This part is where ###############################
##################### you reconst ON center ###########################
  #**print('No of Neurons(ON) in each row greater than {} is {}').format(thresh,nofspikes)
  onspiketimes=newonspiketimes
  #**plt.subplot(1,5,5)
  recon_img = [0]*729
  for i in range(0,len(onspiketimes)):
    if(len(onspiketimes[i])!=0):
      recon_img[i]=1
  recon_img=np.array(recon_img).reshape(27,27)
  #**plt.imshow(recon_img,cmap='gray')
  #**plt.title('Reconst of ON DoG with sigma=' + str(sigma) + ', k=' + str(k))

  #**plt.show() 
  bar.update(v) 

bar.finish()
#**plt.figure(1)
pstool.raster_plot_spike(onallspikes,'*')
plt.show()

plt.figure(2)
pstool.raster_plot_spike(offallspikes,'^')
plt.show()

path = '/home/ruthvik/Desktop/DataSets/DoGdata/Freshdata/test_75spks/'

picklefile1="testing"+str(NoOfImages)+'_Images'+str(separation)+"_separation"+"manual_"+ \
             str(sigma)+"_sigma"+str(k)+"_k_types"+"ON"+"spikelimit"+str(tsteps)+".pkl"
output1 = open(path+picklefile1,'wb')
pickle.dump(onallspikes,output1)
output1.close()
print "pickle file written to",path+picklefile1

picklefile2="testing"+str(NoOfImages)+'_Images'+str(separation)+"_separation"+"manual_"+ \
             str(sigma)+"_sigma"+str(k)+"_k_types"+"OFF"+"spikelimit"+str(tsteps)+".pkl"
output2 = open(path+picklefile2,'wb')
pickle.dump(offallspikes,output2)
output2.close()
print "pickle file written to",path+picklefile2

#picklefile3="train_labels"+str(NoOfImages)+'_Images'+str(tsteps)+"_separation"+"manual_"+ str(sigma)+"_sigma"+str(k)+"_k_types"+"spikelimit"+str(separation)+".pkl"
#output3 = open(path+picklefile3,'wb')
#pickle.dump(train_y,output3)
#output3.close()
#print "pickle file written to",path+picklefile3

