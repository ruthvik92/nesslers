"""This code does the ON and OFF center operations using DoG and it generates
spikes if a pixel value crosses a threshold and also it plots the spike train
the last image, it also reconstructs the image from the spikes by accumulating.
The pixel value is subtracted by threshold and a spike is inserted into positions
wherevever the threshold is crossed. This spike data falls on one timestep only."""
import scipy
import numpy as np
import matplotlib.pyplot as plt
import progressbar
from skimage import data, feature, color, filters, img_as_float
import poisson_tools as pstool
train_x,train_y = pstool.get_train_data()

separation = 0.5

sigma=1
k=2
thresh=21.5
NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
allspikes = [[] for i in range(0,27*27)]
prev_max = 0
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()

########################################################################
#####################This part is where ################################
##################### you reshape the image#############################
####################the to 27x27 and do ################################
####################gaussian filter with################################
####################given k and sigma###################################
for v in range(0,NoOfImages):
  img = train_x[v:v+1]
  de0 = [ i for i in range(0,28)]
  de = [28*i-1 for i in range(2,29)]
  de0.extend(de)
  new_img = np.delete(img,de0)
  new_img = new_img.reshape(1,729)
  img = np.reshape(new_img,(27,27))
  img = color.rgb2gray(img)
  plt.subplot(1,5,1)
  im = plt.imshow(img)
  plt.title('Original Image')
  s1 = filters.gaussian(img,k*sigma)
  s2 = filters.gaussian(img,sigma)
#######################################################################
#####################This part is where ###############################
##################### you calc OFF center #############################

  dog = s1 - s2
  plt.subplot(1,5,2)
  print dog.min(),dog.max()
  plt.imshow(dog,cmap='gray')
  plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))

#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of OFF center filter ############################

  print(sum(sum(dog>thresh)))
  dog=dog-thresh
  nofspikes =sum(sum(dog>0))
  offspikepos = np.argwhere(dog>0)
  dog[dog<=0]=0
  offspiketimes=[[] for i in range(27*27)]
  for items in offspikepos:
      offspiketimes[27*(items[0])+(items[1])].append(0)
      
 
#######################################################################
#####################This part is where ###############################
##################### you reconst OFF center ##########################
  print('No of Neurons(OFF) in each row greater than {} is {}').format(thresh,nofspikes)
  offspiketimes = offspiketimes ##Replce with offspiketimes to see 
  plt.subplot(1,5,3)               #not modified spiketrains
  recon_img = [1]*729
  for i in range(0,len(offspiketimes)):
    if(len(offspiketimes[i])!=0):
      recon_img[i]=0
  recon_img=np.array(recon_img).reshape(27,27)
  plt.imshow(recon_img,cmap='gray')
  plt.title('Reconst of OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))


#######################################################################
#####################This part is where ###############################
##################### you calc ON center ##############################
  dog1 = s2-s1
  plt.subplot(1,5,4)
  print dog1.min(),dog1.max()
  plt.imshow(dog1,cmap='gray')
  plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k))
  
#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of ON center filter #############################
  print(sum(sum(dog1>thresh)))
  dog1 = dog1-thresh
  nofspikes =sum(sum(dog1>0))
  onspikepos = np.argwhere(dog1>0)
  dog[dog<=0]=0
  onspiketimes=[[] for i in range(27*27)]
  for items in onspikepos:
      onspiketimes[27*(items[0])+(items[1])].append(0)
      
#######################################################################
#####################This part is where ###############################
##################### you reconst ON center ###########################
  print('No of Neurons(ON) in each row greater than {} is {}').format(thresh,nofspikes)
  onspiketimes=onspiketimes
  plt.subplot(1,5,5)
  recon_img = [0]*729
  for i in range(0,len(onspiketimes)):
    if(len(onspiketimes[i])!=0):
      recon_img[i]=1
  recon_img=np.array(recon_img).reshape(27,27)
  plt.imshow(recon_img,cmap='gray')
  plt.title('Reconst of ON DoG with sigma=' + str(sigma) + ', k=' + str(k))

  plt.show() 
  bar.update(v) 

bar.finish()
pstool.raster_plot_spike(onspiketimes,'*')
pstool.raster_plot_spike(offspiketimes,'^')
plt.show()

