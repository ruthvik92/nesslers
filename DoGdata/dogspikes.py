"""This code just makes spikes from DoG operations and stores them in an array
, it doesn't plot though."""
import scipy
import numpy as np
import matplotlib.pyplot as plt
import progressbar
from skimage import data, feature, color, filters, img_as_float
import poisson_tools as pstool
train_x,train_y = pstool.get_train_data()


NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
allspikes = [[] for i in range(0,27*27)]
prev_max = 0
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()


for v in range(0,NoOfImages):
  img = train_x[v:v+1]
  de0 = [ i for i in range(0,28)]
  de = [28*i-1 for i in range(2,29)]
  de0.extend(de)
  new_img = np.delete(img,de0)
  new_img = new_img.reshape(1,729)
  img = np.reshape(new_img,(27,27))
  img = color.rgb2gray(img)
  plt.subplot(1,3,1)
  im = plt.imshow(img)
  plt.title('Original Image')
  sigma=1
  k=5
  thresh=30
  s1 = filters.gaussian(img,k*sigma)
  s2 = filters.gaussian(img,sigma)

	# multiply by sigma to get scale invariance
  dog = s1 - s2
  plt.subplot(1,3,2)
  print dog.min(),dog.max()
  plt.imshow(dog,cmap='gray')
  plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))
  print(sum(sum(dog<-30)))
  dog=dog+30
  nofspikes =sum(sum(dog<1))
  offspikepos = np.argwhere(dog<1)
  offspiketimes=[[] for i in range(27*27)]
  for items in offspikepos:
    offspiketimes[(items[0])*(items[1])].append((1./abs(dog[items[0]][items[1]]))*30)
  #offspiketimes =[(1./dog[items[0]][items[1]])*30 for items in spikepos]
  print('No of Neurons(OFF) in each row greater than {} is {}').format(30,nofspikes)

  dog1 = s2-s1
  plt.subplot(1,3,3)
  print dog1.min(),dog1.max()
  plt.imshow(dog1,cmap='gray')
  plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k))
  print(sum(sum(dog1>30)))
  dog1 = dog1-thresh
  nofspikes =sum(sum(dog1>1))
  onspikepos = np.argwhere(dog1>1)
  onspiketimes=[[] for i in range(27*27)]
  for items in onspikepos:
    onspiketimes[(items[0])*(items[1])].append((1./dog1[items[0]][items[1]])*30)
  #onsptimes =[(1./dog1[items[0]][items[1]])*30 for items in spikepos]
  print('No of Neurons(ON) in each row greater than {} is {}').format(30,nofspikes)
  plt.show() 



  
  bar.update(v) 

bar.finish()

