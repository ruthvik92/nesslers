import pickle
import poisson_tools as pstool
import matplotlib.pyplot as plt

output = open('/home/ruthvik/DoG data/test_stimulus2_Images30_separationmanual_1_sigma2_k_typesOFF.pkl','rb')
off = pickle.load(output)
output.close()

output = open('/home/ruthvik/DoG data/test_stimulus2_Images30_separationmanual_1_sigma2_k_typesON.pkl','rb')
on = pickle.load(output)
output.close()

pstool.raster_plot_spike(on,'*')
plt.show()

plt.figure(2)
pstool.raster_plot_spike(off,'^')
plt.show()
