"""This code does the ON and OFF center operations using DoG and it generates
spikes if a pixel value crosses a threshold and also it plots the spike train
the last image, it also reconstructs the image from the spikes by inverting the
neuron firing time to obtain pixel intensity, The pixel value is subtracted by
threshold and inverted and scaled with tsteps to make sure that an image lies within
tsteps number of timesteps, this code doesn't have any limit on the number of spikes
per image"""
import cv2
import glob
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy
import numpy as np
import matplotlib.pyplot as plt
import progressbar
from copy import deepcopy
from skimage import data, feature, color, filters, img_as_float
import poisson_tools as pstool
from sklearn.preprocessing import normalize
path ='/home/ruthvik/Desktop/Summer 2017/\
SDNN_STDP_Masq_Saeed/DataSet/Caltech/Gray_Images/Faces_gray_cropped/'
#files1=os.listdir(path)
files = glob.glob(path+'*.jpg')
size = 201
dim=(size,size)
sigma=1
k=2
thresh= 6
tsteps=30   ##No of spikes per image
separation = 40 ##should be greater than tsteps
NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
allspikes = [[] for i in range(0,size*size)]
prev_max = 0
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
offallspikes = [[] for i in range(0,size*size)]
onallspikes = [[] for i in range(0,size*size)]
for v in range(NoOfImages):
    img=mpimg.imread(files[v])
    img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    plt.subplot(1,5,1)
    plt.imshow(img,cmap='gray')
    plt.title('Original Image')
    s1 = filters.gaussian(img,k*sigma)
    s2 = filters.gaussian(img,sigma)
    s1*=255
    s2*=255
#######################################################################
#####################This part is where ###############################
##################### you calc OFF center #############################

    dog = s1 - s2
    plt.subplot(1,5,2)
    print dog.min(),dog.max()
    plt.imshow(dog,cmap='gray')
    plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))
    
#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of OFF center filter ############################

    print(sum(sum(dog>thresh)))
    dog=dog-thresh
    nofspikes =sum(sum(dog>0))
    offspikepos = np.argwhere(dog>0) ##corrdinates whose value >0
    dog[dog<=0]=0
    offspiketimes=[[] for i in range(size*size)]
    for items in offspikepos:
        x=1./abs(dog[items[0]][items[1]])
        if(x<=1):
            offspiketimes[size*(items[0])+(items[1])].append(x*tsteps+separation*v)
        
########Appending all OFF center spikes ##############################
    for i in range(0,len(offallspikes)):
        offallspikes[i].extend(offspiketimes[i])
    
#######################################################################
#####################This part is where ###############################
##################### you reconst OFF center ##########################
    print('No of Neurons(OFF) in each row greater than {} is {}').format(thresh,nofspikes)
    offspiketimes = offspiketimes 
    plt.subplot(1,5,3)               #not modified spiketrains
    recon_img = [1]*size**2
    for i in range(0,len(offspiketimes)):
        if(len(offspiketimes[i])!=0):
            recon_img[i]=0     ##This will just accumulate the spikes without regard to strength of pixel encoded in time of arrival
            #recon_img[i]=offspiketimes[i][0]  # Since we're reconst'ing the image with 0s, the lower the time,lighter the pixel
    recon_img=np.array(recon_img).reshape(size,size) #you don't do the inverse cos, we didn't invert before
    plt.imshow(recon_img,cmap='gray')
    plt.title('Reconst of OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))

#######################################################################
#####################This part is where ###############################
##################### you calc ON center ##############################
    dog1 = s2-s1
    plt.subplot(1,5,4)
    print dog1.min(),dog1.max()
    plt.imshow(dog1,cmap='gray')
    plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k))
  
#######################################################################
#####################This part is where ###############################
##################### you calc spiketrains ############################
##################### of ON center filter #############################
    print(sum(sum(dog1>thresh)))
    dog1 = dog1-thresh
    nofspikes =sum(sum(dog1>0))
    onspikepos = np.argwhere(dog1>0)
    dog1[dog1<=0]=0
    onspiketimes=[[] for i in range(size*size)]
    for items in onspikepos:
        y=1./dog1[items[0]][items[1]]
        if(y<=1):
            onspiketimes[size*(items[0])+(items[1])].append(y*tsteps+separation*v)
      
########Appending all ON center spikes ###############################
    for i in range(0,len(onallspikes)):
        onallspikes[i].extend(onspiketimes[i])
#######################################################################
#####################This part is where ###############################
##################### you reconst ON center ###########################
    print('No of Neurons(ON) in each row greater than {} is {}').format(thresh,nofspikes)
    onspiketimes=onspiketimes
    plt.subplot(1,5,5)
    recon_img = [0]*size**2
    for i in range(0,len(onspiketimes)):
        if(len(onspiketimes[i])!=0):
            recon_img[i]=1
            #recon_img[i]=onspiketimes[i][0] 
    recon_img=np.array(recon_img).reshape(size,size)
    plt.imshow(recon_img,cmap='gray')
    plt.title('Reconst of ON DoG with sigma=' + str(sigma) + ', k=' + str(k))
    plt.show() 
    bar.update(v) 

bar.finish()
pstool.raster_plot_spike(onallspikes,'*')
pstool.raster_plot_spike(offallspikes,'^')
plt.show()

picklefile1="faces_train_stimulus_orig"+str(NoOfImages)+'_Images'+str(tsteps)+"_separation"+"manual_"+ str(sigma)+"_sigma"+str(k)+"_k_types"+"ON"+"_spklimit"+".pkl"
path = '/home/ruthvik/DoGdata/'
output1 = open(path+picklefile1,'wb')
pickle.dump(onallspikes,output1)
output1.close()
print "pickle file written to",path+picklefile1

picklefile2="faces_train_stimulus_orig"+str(NoOfImages)+'_Images'+str(tsteps)+"_separation"+"manual_"+ str(sigma)+"_sigma"+str(k)+"_k_types"+"OFF"+"_spklimit"+".pkl"
output2 = open(path+picklefile2,'wb')
pickle.dump(offallspikes,output2)
output2.close()
print "pickle file written to",path+picklefile2

