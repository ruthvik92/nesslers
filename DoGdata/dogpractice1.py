
"""This is just DoG from the dogpractice.py without flashy things"""

from skimage import data, feature, color, filters, img_as_float
from matplotlib import pyplot as plt


original_image = img_as_float(data.chelsea())
img = color.rgb2gray(original_image)

k = 2

plt.subplot(1,3,1)
plt.imshow(original_image)
plt.title('Original Image')

sigma=1
s1 = filters.gaussian(img,k*sigma)
s2 = filters.gaussian(img,sigma)

	# multiply by sigma to get scale invariance
dog = s1 - s2
plt.subplot(1,3,2)
print dog.min(),dog.max()
plt.imshow(dog,cmap='gray')
plt.title('ON DoG with sigma=' + str(sigma) + ', k=' + str(k))

dog1 = s2-s1
plt.subplot(1,3,3)
print dog1.min(),dog1.max()
plt.imshow(dog1,cmap='gray')
plt.title('OFF DoG with sigma=' + str(sigma) + ', k=' + str(k))
plt.show()
