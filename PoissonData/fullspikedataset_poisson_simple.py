import pickle
import math
import random
import sys
import matplotlib.pyplot as plt
import numpy as np
import os
import poisson_tools as pstool
import progressbar
train_x,train_y = pstool.get_train_data()
random.seed(10)
image_size = 27
rate = 30
duration = 40 # in ms
timestep = 0.001 ##in seconds
NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
separation= int(raw_input('Enter seperation between spikes patterns that you want(n): '))
recon= int(raw_input('Do you want reconstruction? Enter 1 if yes, else 0: '))
allspikes = [[] for i in range(0,image_size*image_size)]
prev_max = 0
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
for v in range(0,NoOfImages):
    img = train_x[v:v+1]
    de0 = [ i for i in range(0,28)]
    de = [28*i-1 for i in range(2,29)]
    de0.extend(de)
    new_img = np.delete(img,de0)
    new_img = new_img.reshape(1,729)
    
    #im = plt.imshow(np.reshape(new_img,(27,27)),cmap='gray')
    #plt.show()
    #spikes = pstool.nessler_poisson_disc(new_img, rate, duration,separation,v, timestep)
    spikes = pstool.nessler_poisson_cont(new_img, image_size, image_size, rate, duration, separation,v)
    if(recon):
        recon_img = [255]*729
        for i in range(0,len(spikes)):
            if(len(spikes[i])!=0):
                recon_img[i]=spikes[i][0]-v*(duration+separation)
        recon_img=np.array(recon_img).reshape(27,27)
        plt.imshow(recon_img,cmap='gray',interpolation=None)
        plt.title('Reconstruction with rate='+str(rate))
        plt.show()
    
    for i in range(0,len(allspikes)):
        allspikes[i].extend(spikes[i])

    bar.update(v)
    
bar.finish()
pstool.raster_plot_spike(allspikes, marker='*', markersize=4,color='b')
plt.show()
picklefile="stimulus_cont"+str(NoOfImages)+"Images"+str(rate)+"rate"+str(duration)+"dur"+"sep"+str(separation)+".pkl"
path = '/home/ruthvik/Desktop/DataSets/PoissonData/simple/'
output = open(path+picklefile,'wb')
pickle.dump(allspikes,output)
output.close()
print 'pickle file written to',path+picklefile

'''spikes=[len(items) for items in allspikes if len(items)!=0]
ys=[]
for i in range(1,8):
    ys.append(spikes.count(i))
plt.scatter([i for i in range(1,8)],ys)
plt.xlabel('Number of spikes')
plt.ylabel('Number of neurons')
plt.title('Spikes with sampling from exp')
plt.show()'''

