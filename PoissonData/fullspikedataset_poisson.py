import pickle
import math
import random
import sys
import matplotlib.pyplot as plt
import numpy as np
import os
import poisson_tools as pstool
import progressbar
train_x,train_y = pstool.get_train_data()
random.seed(10)
image_size = 27
max_rate = 2500.
duration = 40 #ms, 100s
silence = 000 #ms
NoOfImages= int(raw_input('Enter No of repetitions of spike patterns you want(n): '))
Separation= int(raw_input('Enter seperation between spikes patterns that you want(n): '))
recon= int(raw_input('Do you want reconstruction? Enter 1 if yes, else 0: '))
allspikes = [[] for i in range(0,27*27)]
prev_max = 0
bar = progressbar.ProgressBar(maxval=NoOfImages, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
bar.start()
for v in range(0,NoOfImages):
    img = train_x[v:v+1]
    de0 = [ i for i in range(0,28)]
    de = [28*i-1 for i in range(2,29)]
    de0.extend(de)
    new_img = np.delete(img,de0)
    new_img = new_img.reshape(1,729)
    
    #plt.imshow(np.reshape(new_img,(27,27)),cmap='gray')
    #path2 = "/home/ruthvik/Desktop/28by28.jpeg"  
    #plt.show()
    #plt.imsave(path2,(np.reshape(new_img,(27,27))))
    spikes = pstool.mnist_poisson_gen(new_img, image_size, image_size, max_rate, duration, silence)
    offset = min([min(items) for items in spikes if len(items)!=0])

    for items in spikes:
        if len(items)!=0:
            spikes[spikes.index(items)]=[x-offset for x in items]
   

    allspikes2 = [[x+(prev_max) for x in items] for items in spikes]
    if(recon):
        recon_img = [255]*729
        for i in range(0,len(spikes)):
            if(len(spikes[i])!=0):
                recon_img[i]=spikes[i][0]
        recon_img=np.array(recon_img).reshape(27,27)
        plt.imshow(recon_img,cmap='gray',interpolation=None)
        plt.title('Reconstruction with max_rate='+str(max_rate))
        plt.show()
    
    for i in range(0,len(allspikes)):
        allspikes[i].extend(allspikes2[i])


    prev_max = max([max(items) for items in allspikes if len(items)!=0])+Separation
    bar.update(v)    
bar.finish()
pstool.raster_plot_spike(allspikes, marker='*', markersize=4,color='b')
plt.show()
'''picklefile="stimulus"+str(NoOfImages)+"Images"+str(max_rate)+"rate"+str(duration)+"dur"+"sep"+str(Separation)+".pkl"
path = '/home/ruthvik/Desktop/DataSets/PoissonData/'
output = open(path+picklefile,'wb')
pickle.dump(allspikes,output)
output.close()
print 'pickle file written to',path+picklefile'''
